//
//  CustomPastOrderTableViewCell.swift
//  business
//
//  Created by praveen kumar on 12/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class CustomPastOrderTableViewCell: UITableViewCell {

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var tripDate: UILabel!
    @IBOutlet weak var vehicleName: UILabel!
}
