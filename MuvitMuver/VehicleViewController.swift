//
//  VehicleViewController.swift
//  MuvitMuver
//
//  Created by praveen kumar on 23/09/16.
//  Copyright © 2016 Muvit Technologies. All rights reserved.
//

import UIKit

class VehicleViewController: UITableViewController {
    
    
    var vehicles = [Dictionary<String,AnyObject>]()
    var deletedIndex = -1
    var defaultIndex = -1
    
    @IBOutlet weak var menuButton:UIBarButtonItem!
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "changerequestseague" {
            let vc = segue.destination as! RequestNavigationController
            //vc.currentUserSecondVC = !currentUser //you can do whatever you want with it in the 2nd VC
            self.present(vc, animated: true, completion: nil)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        //tableView.separatorColor = UIColor(patternImage: UIImage(named: "YOUR_IMAGE_NAME")!)
        
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [
            "pageno" : "1"
        ]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "gva")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
     
    //Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        return self.vehicles.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        let cell=tableView.dequeueReusableCell(withIdentifier: "vehiclecell", for: indexPath) as! VehicleTableViewCell
        let vehicle = vehicles[(indexPath as NSIndexPath).row]
        let vehicleId = Int(Constant.anyToString(vehicle["vehicleid"]!))
        let modelYear = Int(Constant.anyToString(vehicle["modelyear"]!))
        
            if let vehicleNumber = vehicle["vehiclenumber"] as? String{
                    if let name = vehicle["name"] as? String{
                        if let url = vehicle["url"] as? String{
                            cell.initMe((indexPath as NSIndexPath).row, parent: self, vehicleId: vehicleId!, vehicleNumber: vehicleNumber, modelYear: modelYear!, url: url, name: name)
                        }
                    }
                
            }
        
        return cell
    }
    
    func setDefaultVehicle(){
        LoginModel.defaultVehicle = vehicles[defaultIndex]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        defaultIndex = (indexPath as NSIndexPath).row
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [
            "vehicleid" : String(describing: vehicles[(indexPath as NSIndexPath).row])
        ]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "cv")
        
    }
    
    func deleteVehicle(_ index: Int){
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        deletedIndex = index
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [
            "vehicleid" : String(describing: vehicles[index])
        ]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "dv")
    }
    
    func deleteFromTableView(){
        vehicles.remove(at: deletedIndex)
    }
    
    func reloadDataWithArray(_ vehicles:[Dictionary<String,AnyObject>]){
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        self.vehicles = vehicles
        self.tableView.reloadData()
    }

}
