//
//  OrderSummaryTableViewController.swift
//  SidebarMenu
//
//  Created by Kapil Rathore on 28/08/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit

class OrderSummaryTableViewController: UITableViewController {
    //@IBOutlet weak var :UIBarButtonItem!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var orders = [Dictionary<String,AnyObject>]()
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        //Constant.orderSummaryTableViewController = self
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [:]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "fct")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        return self.orders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let cell=tableView.dequeueReusableCell(withIdentifier: "customordercell", for: indexPath) as! CustomOrderCell
        if let orderid = self.orders[(indexPath as NSIndexPath).row]["orderid"] as? String {
            cell.orderLabelId.text=(orderid)
        }
        if let status = self.orders[(indexPath as NSIndexPath).row]["status"] as? String {
            cell.status.text=status
        }
        if let moverInfo = (self.orders[(indexPath as NSIndexPath).row]["movername"] as? String){
            cell.moverInfo = moverInfo
            if let movernumber = (self.orders[(indexPath as NSIndexPath).row]["movernumber"] as? String){
                cell.moverInfo += ("\n" + movernumber)
            }
        }
        cell.uiViewController = self
        
        return cell
    }
//    
//    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
//        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
//        
//    }
    
    func reloadDataWithArray(_ arr:[Dictionary<String,AnyObject>]){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        self.orders = arr
        self.tableView.reloadData()
    }
    
    func reloadNotificationDataWithArray(_ arr:[Dictionary<String,AnyObject>],name: String,mobileno: String){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        for i in 0..<self.orders.count {
            for j in 0..<arr.count {
                let orderid = arr[j]["orderid"] as! String
                if orderid == self.orders[i]["orderid"] as! String {
                    self.orders[i]["status"] = arr[j]["status"] as! String as AnyObject?
                    self.orders[i]["movername"] = name as AnyObject?
                    self.orders[i]["movernumber"] = mobileno as AnyObject?
                }
            }
        }
        
        self.tableView.reloadData()
    }

    func reloadNotificationStatusDataWithArray(_ orderid: String, status: String, message: String = ""){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        if message != "" {
            let notification:UILocalNotification = UILocalNotification()
            notification.alertBody = message
            notification.fireDate = Date()
            UIApplication.shared.scheduleLocalNotification(notification)
        }
        
        for i in 0..<self.orders.count {
            if orderid == self.orders[i]["orderid"] as! String {
                    self.orders[i]["status"] = status as AnyObject?
            }
        }
        
        self.tableView.reloadData()
    }
    
    func dismissMe(){
        self.dismiss(animated: true, completion: nil)
    }
}
