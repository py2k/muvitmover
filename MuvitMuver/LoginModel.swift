//
//  LoginModel.swift
//  business
//
//  Created by praveen kumar on 08/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

class LoginModel {
    
    static var email=""
    static var mobileno=""
    static var firstname=""
    static var lastname=""
    static var lat=0.0
    static var lng=0.0
    static var address=""
    static var sessionkey=""
    static var meterupdatetime=0
    static var deliverymodetime=""
    static var PASTORDERLIMIT=7
    static var currentLat=0.0
    static var currentLng=0.0
    static var currentMode=Constant.CAR
    static var subscribedmode=Constant.CAR
    static var currentDeliveryNowOrLater=Constant.NOW
    static var moverstatus=Constant.ASSIGNED
    static var imageurl=""
    static var defaultVehicle = Dictionary<String,AnyObject>()
    
    static func saveInDefaults(){
        
        Constant.defaults.set(email, forKey: "email")
        Constant.defaults.set(mobileno, forKey: "mobileno")
        Constant.defaults.set(firstname, forKey: "firstname")
        Constant.defaults.set(lastname, forKey: "lastname")
        Constant.defaults.set(lat, forKey: "lat")
        Constant.defaults.set(lng, forKey: "lng")
        Constant.defaults.set(address, forKey: "address")
        Constant.defaults.set(sessionkey, forKey: "sessionkey")
        Constant.defaults.set(meterupdatetime, forKey: "meterupdatetime")
        Constant.defaults.set(moverstatus, forKey: "moverstatus")
        Constant.defaults.set(currentLat, forKey: "currentLat")
        Constant.defaults.set(currentLng, forKey: "currentLng")
        Constant.defaults.set(Date(), forKey: "logintime")
        Constant.defaults.set(subscribedmode, forKey: "subscribedmode")
    }
    
    static func loadDataFromDefaults(){
        
        email = Constant.defaults.object(forKey: "email") as! String
        mobileno = Constant.defaults.object(forKey: "mobileno") as! String
        firstname = Constant.defaults.object(forKey: "firstname") as! String
        lastname = Constant.defaults.object(forKey: "lastname") as! String
        lat = Constant.defaults.object(forKey: "lat") as! Double
        lng = Constant.defaults.object(forKey: "lng") as! Double
        address = Constant.defaults.object(forKey: "address") as! String
        sessionkey = Constant.defaults.object(forKey: "sessionkey") as! String
        meterupdatetime = Constant.defaults.object(forKey: "meterupdatetime") as! Int
        subscribedmode = Constant.defaults.object(forKey: "subscribedmode") as! Int
        moverstatus = Constant.defaults.object(forKey: "moverstatus") as! Int
    }

}
