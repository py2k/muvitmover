//
//  RouteUrl.swift
//  muv
//
//  Created by praveen kumar on 12/08/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import Foundation

class RouteUrl {

    static let BASEURL = "http://192.168.0.15/akhil/akhil/mover/process.php?pto="
    static let reg = BASEURL+"reg"
    static let ae = BASEURL+"ae"
    static let age = BASEURL+"age"
    static let am = BASEURL+"am"
    static let agm=BASEURL+"agm"
    static let ad=BASEURL+"ad"
    static let lo=BASEURL+"lo"
    static let oc=BASEURL+"oc"
    static let rlo=BASEURL+"rlo"
    static let uma=BASEURL+"uma"
    static let uml=BASEURL+"uml"
    static let gti=BASEURL+"gti"
    static let icc=BASEURL+"icc"
    static let gccl=BASEURL+"gccl"
    static let dcc=BASEURL+"dcc"
    static let sdc=BASEURL+"sdc"
    static let cn=BASEURL+"cn"
    static let dco=BASEURL+"dco"
    static let fct = BASEURL+"fct"
    static let prt=BASEURL+"prt"
    static let prmt=BASEURL+"prmt"
    static let memr=BASEURL+"memr"
    static let mscr=BASEURL+"mscr"
    static let cqg=BASEURL+"cqg"
    static let mqg=BASEURL+"mqg"
    static let ucp=BASEURL+"ucp"
    static let ump=BASEURL+"ump"
    static let cqs=BASEURL+"cqs"
    static let mqs=BASEURL+"mqs"
    static let ca=BASEURL+"ca"
    static let uoa=BASEURL+"uoa"
    static let ccv=BASEURL+"ccv"
    static let cms=BASEURL+"cms"
    static let spp=BASEURL+"spp"
    
    static let aco = BASEURL+"aco"
    static let cp=BASEURL+"cp"
    static let fco=BASEURL+"fco"
    static let av=BASEURL+"av"
    static let dv=BASEURL+"dv"
    static let cv=BASEURL+"cv"
    static let gva=BASEURL+"gva"
    static let cco=BASEURL+"cco"
    
}