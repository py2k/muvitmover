//
//  TimerHandle.swift
//  business
//
//  Created by praveen kumar on 16/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import Foundation

class TimerHandle{
    
    static var timer: DispatchSource?
    static var state=0;
    
    static func startTimer() {
        Constant.log("Enter "+String(state),fileName: #file, functionName: #function, atLine: #line)
       
    }
    
    static func stopTimer() {
        if timer != nil{
            timer!.cancel()
        }
        timer = nil
    }
    
}
