//
//  StartViewController.swift
//  muv
//
//  Created by praveen kumar on 23/06/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    @IBOutlet weak var startButtonLogin: UIButton!
    @IBOutlet weak var startButtonSignUp: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        startButtonLogin.layer.cornerRadius = 5
        startButtonLogin.layer.borderWidth = 1
        startButtonLogin.layer.borderColor = UIColor.white.cgColor
        startButtonSignUp.layer.cornerRadius = 5
        startButtonSignUp.layer.borderWidth = 1
        startButtonSignUp.layer.borderColor = UIColor.white.cgColor
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }

    /*/Users/praveenkumar/Desktop/pg/muv/muv/StartViewController.swift
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
