//
//  ChangeAddressNavigationController.swift
//  business
//
//  Created by praveen kumar on 13/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class ChangeAddressNavigationController: UINavigationController {

    
    func setIdentityValue(_ identity:String,orderid: String = ""){
        
        let changeAddressViewController = self.viewControllers[0] as! ChangeAddressViewController
        changeAddressViewController.setIdentityValue(identity,orderid: orderid)
    }
    
}
