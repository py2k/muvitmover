//
//  DisclaimerViewController.swift
//  muv
//
//  Created by praveen kumar on 12/08/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class DisclaimerViewController: UIViewController,WebServiceDelegate {
    
    var email:String = ""
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    //let validator = Validator()
    var tosValue:String = ""
    @IBOutlet weak var tosTextView: UITextView!
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        tosTextView.text = tosValue
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    func setTosField(_ tos:String){
            tosValue = tos
    }
    func processWebService(_ data: Data?, response: URLResponse?, error: Error?,identity: NSString){
        
        self.actInd.stopAnimating()
        self.loadingView.removeFromSuperview()
        self.container.removeFromSuperview()
        
        // 1: Check HTTP Response for successful GET request
        guard let httpResponse = response as? HTTPURLResponse, let _ = data
            else {
                print("error: not a valid http response")
                return
        }
        
        switch (httpResponse.statusCode)
        {
        case 200:
            
            //let response = NSString (data: receivedData, encoding: NSUTF8StringEncoding)
        
            do{
                if data != nil {
                    let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                    print(dic)
                    let status = dic["status"] as! String
                    print(status)
                    let pto = dic["pto"] as! String
                    if status == "success" && pto == "approve" {
                         DispatchQueue.main.async(execute: {
                            let approveString = "Your requested has been forwarded to admim"
                            
                            //self.dismissViewControllerAnimated(true, completion: nil)
                            let alertController = UIAlertController(title: "Message", message: approveString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                // self.createPopUp(dic)
                                 exit(0)
                            }
                            alertController.addAction(confirmAction)
                            self.present(alertController, animated: true, completion: nil)
                         });
                    }
                }
            }
            catch{
                print("reg error response")
                Constant.internetNotWorking(self)
            }
        
            
            
        default:
            print("save profile POST request got response \(httpResponse.statusCode)")
        }
    }
    
    
    @IBAction func disclaimerRead(_ sender: AnyObject) {
        Constant.showActivityIndicatory(self.view,actInd: self.actInd,loadingView: self.loadingView,container: self.container)
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            let c = Constant()
            c.webServiceDelegate=self
            let dictionary: [String:String] = [
                "email" : self.email
            ]
            
            c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.ad as NSString,identity: "ad")
        });
    }

}
