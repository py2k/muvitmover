
//
//  LaunchScreenViewController.swift
//  business
//
//  Created by praveen kumar on 17/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    var state:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.setCurrentController(self)
        // Do any additional setup after loading the view, typically from a nib.
        if (CheckCurrentController.launchScreenViewController() == true){
        print("hellop")
        }
        let timeDelay = DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: timeDelay) {
            let defaults = Constant.defaults
            if defaults.object(forKey: "sessionkey") != nil {
                
                LoginModel.loadDataFromDefaults()
                OrderWebService.storyboardHandler(self,pto: "allvehicles")
            }
            else{
            
                OrderWebService.storyboardHandler(self,pto: "startentry")
            }
            
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
