//
//  changeAddressViewController.swift
//  business
//
//  Created by praveen kumar on 13/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ChangeAddressViewController: UIViewController,UISearchBarDelegate,LocateOnTheMap,CLLocationManagerDelegate {
    
    var googleMapsView: GMSMapView!
    @IBOutlet weak var googleMapsContainer: UIView!
    var searchResultsController: SearchResultsController!
    var searchController: UISearchController!
    var resultsArray = [String]()
    let locationManager=CLLocationManager()
    var identity = "ca"
    var searchAddress:String = ""
    var lat:String = ""
    var lng:String = ""
    var orderid = ""
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func setIdentityValue(_ identity: String,orderid:String = ""){
        self.identity = identity
        self.orderid = orderid
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        // Do any additional setup after loading the view, typically from a nib.
        self.locationManager.delegate=self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        Constant.setCurrentController(self)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        searchResultsController = SearchResultsController()
        searchResultsController.delegate = self
        searchController = UISearchController(searchResultsController:searchResultsController)
        //searchController.searchResultsController = searchResultsController
        searchController.searchBar.sizeToFit()
        searchController.searchBar.placeholder = "Search for places"
        navigationItem.titleView = searchController.searchBar
        searchController.searchBar.delegate=self
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        
        if self.googleMapsView == nil {
            self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
            self.googleMapsView.settings.myLocationButton=true
            self.googleMapsView.isMyLocationEnabled=true
            self.view.insertSubview(self.googleMapsView, at: 0)
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let lat = self.locationManager.location?.coordinate.latitude
        let lon = self.locationManager.location?.coordinate.longitude
        locateWithLongitude(lon!, andLatitude: lat!, andTitle: "New Location")
        self.locationManager.stopUpdatingLocation()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        print(searchText)
        //let searchText = sender.attributedText!.string
        let placesClient = GMSPlacesClient()
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil){ (results,error: Error?) -> Void in
            self.resultsArray.removeAll()
            if results == nil{
                return
            }
            //print(results)
            for result in results! {
                //print(result.attributedFullText.string)
                self.resultsArray.append(result.attributedFullText.string)
                
            }
            print(self.resultsArray)
            self.searchResultsController.reloadDataWithArray(self.resultsArray)
        }
        
    }
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    
    @IBAction func setAddressButtonPressed(_ sender: AnyObject) {
        let orderWebService = OrderWebService()
        var dic: [String:String] = [
            "lat" : self.lat,
            "lng" : self.lng,
            "address"  : self.searchAddress
        ]
        if identity == "uoa" {
            dic["orderid"] = orderid
        }
        orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: identity)
    }
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        DispatchQueue.main.async{() -> Void in
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position:position)
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 14)
            
            if self.googleMapsView == nil {
                self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
                self.googleMapsView.settings.myLocationButton=true
                self.googleMapsView.isMyLocationEnabled=true
                self.view.insertSubview(self.googleMapsView, at: 0)
            }
            self.googleMapsView.camera=camera
            
            marker.title=title
            marker.map=self.googleMapsView
            
            self.lat = String(lat)
            self.lng = String(lon)
            self.searchAddress = title
            
        }
    }
    
    func dismissMe(){
        self.dismiss(animated: true, completion: nil)
    }
    
}
