//
//  ExtraViewController.swift
//  business
//
//  Created by praveen kumar on 14/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class ExtraViewController: UIViewController {
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
}
