//
//  AddVehicleViewController.swift
//  MuvitMuver
//
//  Created by praveen kumar on 27/09/16.
//  Copyright © 2016 Muvit Technologies. All rights reserved.
//

import UIKit

class AddVehicleViewController: UIViewController, UITextFieldDelegate,  UIImagePickerControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var vehicleName: UITextField!
    @IBOutlet weak var vehicleNumber: UITextField!
    @IBOutlet weak var vehicleNumberLabel: UILabel!
    @IBOutlet weak var vehicleType: UITextField!
    @IBOutlet weak var vehicleTypeLabel: UILabel!
    @IBOutlet weak var modelYear: UITextField!
    @IBOutlet weak var modelYearLabel: UITextField!
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    var pickedImage:UIImage?
    
    @IBOutlet weak var imageControl: UIButton!
    var pickOption = ["Car","Truck"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pickerView = UIPickerView()
        
        pickerView.delegate = self
        
        vehicleType.inputView = pickerView
        
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    // Move the text field in a pretty animation!
    func moveLabel(_ textField: UILabel, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField != vehicleName {
            moveTextField(textField, moveDistance: -240, up: true)
        }
        
    }
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField != vehicleName {
            moveTextField(textField, moveDistance: -240, up: false)
        }
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        textField.resignFirstResponder()
        return true;
    }
    
    @IBAction func backPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func hideAllKeyboard(_ sender: AnyObject) {
        hideAllFieldKeyboard()
    }
    func hideAllFieldKeyboard(){
        vehicleName.resignFirstResponder()
        vehicleNumber.resignFirstResponder()
        vehicleType.resignFirstResponder()
        modelYear.resignFirstResponder()
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    
    @IBAction func imageControlPressed(_ sender: AnyObject) {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            //imageControl.contentMode = .scaleAspectFit
            imageControl.setImage(pickedImage, for: UIControlState())
            
            self.pickedImage = pickedImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addVehiclePressed(_ sender: AnyObject) {
        if (pickedImage != nil){
            var vehicleTypeId = Constant.CAR
            if self.vehicleType.text == "Pick Up" {
                vehicleTypeId = Constant.TRUCK
            }
            let orderWebService = OrderWebService()
            let dic: [String:String] = [
                "name" : self.vehicleName.text!,
                "vehiclenumber" : self.vehicleNumber.text!,
                "vehicletypeid"  : String(vehicleTypeId),
                "modelyear" : self.modelYear.text!
            ]
             orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "av")
        }
        else{
            Constant.showDialog(self, message: "Error", title: "Please select the vehicle image", completionHandler: nil)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickOption.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickOption[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        vehicleType.text = pickOption[row]
    }
    
}
