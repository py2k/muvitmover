//
//  SignUpViewController.swift
//  muv
//
//  Created by praveen kumar on 24/06/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import SwiftValidator

class SignUpViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate,WebServiceDelegate,ValidationDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var personImageView: UIButton!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var mobilenoTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var isage: CheckBox!
    @IBOutlet weak var l1: UILabel!
    @IBOutlet weak var l2: UILabel!
    @IBOutlet weak var l3: UILabel!
    @IBOutlet weak var l4: UILabel!
    @IBOutlet weak var l5: UILabel!
    @IBOutlet weak var l6: UILabel!
     let validator = Validator()
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    var data = ["Walking/Biking","Driving"]
    var picker = UIPickerView()
    
    @IBOutlet weak var signUpButton: UIButton!
    
    func checkEnglishPhoneNumberFormat(_ string: String?, str: String?) -> Bool{
        l3.isHidden=true
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.characters.count < 3{
            
            if str!.characters.count == 1{
                
                mobilenoTextField.text = "("
            }
            
        }else if str!.characters.count == 5{
            
            mobilenoTextField.text = mobilenoTextField.text! + ") "
            
        }else if str!.characters.count == 10{
            
            mobilenoTextField.text = mobilenoTextField.text! + "-"
            
        }else if str!.characters.count > 14{
            
            return false
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        return checkEnglishPhoneNumberFormat(string, str: str)
    }
    func hideAllFieldKeyboard(){
        firstnameTextField.resignFirstResponder()
        lastnameTextField.resignFirstResponder()
        emailTextField.resignFirstResponder()
        mobilenoTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        confirmPasswordTextField.resignFirstResponder()
        textField.resignFirstResponder()    
    }
    @IBAction func hideKeyboard(_ sender: AnyObject) {
        hideAllFieldKeyboard()
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        signUpButton.layer.cornerRadius = 5
        signUpButton.layer.borderWidth = 1
        signUpButton.layer.borderColor = UIColor.white.cgColor
        picker.delegate = self
        picker.dataSource = self
        textField.inputView = picker
        l1.isHidden=true
        l2.isHidden=true
        l3.isHidden=true
        l4.isHidden=true
        l5.isHidden=true
        l6.isHidden=true
        
        mobilenoTextField.delegate = self
       
        validator.registerField(firstnameTextField, errorLabel: l1, rules: [RequiredRule()])
        validator.registerField(lastnameTextField, errorLabel: l1, rules: [RequiredRule()])
        validator.registerField(emailTextField, errorLabel: l2, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
        
        validator.registerField(mobilenoTextField, errorLabel: l3, rules: [RequiredRule(), MinLengthRule(length: 14), MaxLengthRule(length: 14)])
        
        validator.registerField(passwordTextField, errorLabel: l4, rules: [RequiredRule(), MinLengthRule(length: 6), MaxLengthRule(length: 20)])
        validator.registerField(confirmPasswordTextField, errorLabel: l5, rules: [ConfirmationRule(confirmField: passwordTextField)])
        

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Constant.drawBottomBorderTextField(firstnameTextField)
        Constant.drawBottomBorderTextField(lastnameTextField)
        Constant.drawBottomBorderTextField(mobilenoTextField)
        Constant.drawBottomBorderTextField(emailTextField)
        Constant.drawBottomBorderTextField(passwordTextField)
        Constant.drawBottomBorderTextField(confirmPasswordTextField)
        Constant.drawBottomBorderTextField(textField)
        //initPlot()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func createPopUp(_ dic: NSDictionary){
        
        DispatchQueue.main.async(execute: {
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "otpverification") as! PopUpViewController
            self.addChildViewController(popOverVC)
            
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
            popOverVC.email = self.emailTextField.text!
            let phoneNumber = self.mobilenoTextField.text!
            let newString = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
            popOverVC.mobileno = newString
            popOverVC.identity = "ae"
            popOverVC.processRegistration(dic)
        });
    }
    
    
    func processWebService(_ data: Data?, response: URLResponse?, error: Error?,identity: NSString){
        
        stopActivityIndicator()
        
        // 1: Check HTTP Response for successful GET request
        guard let httpResponse = response as? HTTPURLResponse, let _ = data
            else {
                print("error: not a valid http response")
                return
        }
        
        switch (httpResponse.statusCode)
        {
        case 200:
            
            //let response = NSString (data: receivedData, encoding: NSUTF8StringEncoding)
            
            if identity == "reg" {
                do{
                    if data != nil {
                        let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                        print(dic)
                        let status = dic["status"] as! String
                        print(status)
                        if dic["pto"] == nil {
                            if status == "fail" {
                                let errorString = dic["error"] as! String
                                 DispatchQueue.main.async(execute: {
                                     self.stopActivityIndicator()
                                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                                    let confirmAction = UIAlertAction(
                                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                                        exit(0)
                                    }
                                    alertController.addAction(confirmAction)
                                    self.present(alertController, animated: true, completion: nil)
                                 });
                            }
                        }
                        else{
                            if status == "success" {
                                self.createPopUp(dic)
                            }
                            else {
                                let errorString = dic["error"] as! String
                                DispatchQueue.main.async(execute: {
                                     self.stopActivityIndicator()
                                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                                    let confirmAction = UIAlertAction(
                                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                                        self.createPopUp(dic)
                                    }
                                    alertController.addAction(confirmAction)
                                    self.present(alertController, animated: true, completion: nil)
                                });
                            }
                        }
                    }
                }//if data finsihh
            catch{
                print("reg error response")
                Constant.internetNotWorking(self)
            }
            
        }//reg finish
        
        
        default:
        print("save profile POST request got response \(httpResponse.statusCode)")
    }
}

func validationSuccessful() {
    let phoneNumber = self.mobilenoTextField.text!
    let newString = phoneNumber.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        print("phoneNumber")
        print(phoneNumber)
        print(newString)
        if newString.characters.count == 10 {
            l3.isHidden=true
            if isage.isChecked == true {
                
                l6.isHidden=true
                // submit the form
                Constant.showActivityIndicatory(self.view,actInd: self.actInd,loadingView: self.loadingView,container: self.container)
                DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                    let c = Constant()
                    c.webServiceDelegate=self
                    var subscribedmode=1
                    if self.textField.text! == "Driving" {
                        subscribedmode=2
                    }
                    let dictionary: [String:String] = [
                        "firstname" : self.firstnameTextField.text!,
                        "lastname" : self.lastnameTextField.text!,
                        "email" : self.emailTextField.text!,
                        "mobileno" : newString,
                        "password" : self.passwordTextField.text!,
                        "subscribedmode" : String(subscribedmode),
                        "currentmode" : String(subscribedmode),
                        "medium" : String(3)
                    ]
                    
                    
                    c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.reg as NSString,identity: "reg")
                });
            
            }
            else{
                l6.text="Confirm your age"
                l6.isHidden=false
            }
            
        }
        else{
            l3.text="Mobile No not valid"
            l3.isHidden=false
        }
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
        //    if let field = field as? UITextField {
                //field.layer.borderColor = UIColor.redColor().CGColor
                //field.layer.borderWidth = 1.0
      //      }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    
    //on submit click
    @IBAction func showOtpVerificationPopup(_ sender: AnyObject) {
        validator.validate(self)
    }
    
    @IBAction func clickPersonImage(_ sender: AnyObject) {
        let photoPicker         = UIImagePickerController()
        photoPicker.delegate    = self
        photoPicker.sourceType  = .photoLibrary
        
        self.present(photoPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        let imagePlay = info[UIImagePickerControllerOriginalImage] as? UIImage
        personImageView.setImage(imagePlay, for: UIControlState())
        self.dismiss(animated: false, completion: nil)
    }     
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        textField.text=data[row]
        self.view.endEditing(true)
        //picker.hidden = true
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return data[row]
    }
    // UITextField Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
}
