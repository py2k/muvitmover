//
//  VehicleTableViewCell.swift
//  MuvitMuver
//
//  Created by praveen kumar on 23/09/16.
//  Copyright © 2016 Muvit Technologies. All rights reserved.
//

import UIKit


class VehicleTableViewCell: UITableViewCell {

    @IBOutlet weak var vehicleImage: UIImageView!
    @IBOutlet weak var vechicleName: UILabel!
    @IBOutlet weak var vehicleNumber: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    var vehicleId = 0
    var parent: VehicleViewController?
    var index = 0
    
    func initMe(_ index: Int, parent: VehicleViewController,vehicleId: Int,vehicleNumber: String, modelYear: Int, url: String,name: String){
        self.index = index
        self.parent = parent
        self.vehicleId = vehicleId
        self.vechicleName.text = name
        if modelYear == 0 && modelYear == 0{
            hideDeleteAndVehicleNumber()
        }
        else{
            self.vehicleNumber.text = vehicleNumber
        }
        if url != "" {
            setVehicleImageView(url)
        }
    }
    
    func hideDeleteAndVehicleNumber(){
        self.deleteButton.isHidden = true
        self.vehicleNumber.isHidden = true
    }
    
    func setVehicleImageView(_ url: String){
        
        if let url = URL(string: url){
            let data = try? Data(contentsOf: url)
            if data != nil {
                vehicleImage?.image = UIImage(data:data!)
            }
        }
    }
    
    @IBAction func deleteVehicle(_ sender: AnyObject) {
        if parent != nil {
            parent!.deleteVehicle(index)
        }
    }
}
