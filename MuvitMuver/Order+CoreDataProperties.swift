//
//  Order+CoreDataProperties.swift
//  
//
//  Created by praveen kumar on 08/09/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Order {

    @NSManaged var orderid: NSNumber?
    @NSManaged var status: String?
    @NSManaged var address: String?
    @NSManaged var movername: String?
    @NSManaged var movernumber: String?
    @NSManaged var isdelivered: NSNumber?

}
