//
//  CustomOrderCell.swift
//  business
//
//  Created by praveen kumar on 11/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class CustomOrderCell: UITableViewCell {

    @IBOutlet weak var orderLabelId: UILabel!
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var updateAddressButton: UIButton!
    
    @IBOutlet weak var dropOrderButton: UIButton!
    
    @IBOutlet weak var moverInfoButton: UIButton!
    var uiViewController: OrderSummaryTableViewController?
    var popTip = AMPopTip()
    var moverInfo: String = ""
    
    @IBAction func updateOrder(_ sender: AnyObject) {
        DispatchQueue.main.async{
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let changeAddressNavigationController = storyboard.instantiateViewController(withIdentifier: "changeaddress") as! ChangeAddressNavigationController
            //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                
            changeAddressNavigationController.setIdentityValue("uoa",orderid: self.orderLabelId.text!)
            self.uiViewController!.present(changeAddressNavigationController, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelOrder(_ sender: AnyObject) {
         let orderWebService = OrderWebService()
        let dictionary: [String:String] = [:]
        orderWebService.executeWebService(uiViewController!, dic: dictionary as NSDictionary, identity: "cco")
        
    }
    
    @IBAction func moverInfoClicked(_ sender: AnyObject) {
        
        popTip.shouldDismissOnTap = true
        popTip.edgeMargin = 5
        popTip.offset = 2
        popTip.edgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        popTip.popoverColor = UIColor.orange
        popTip.showText(moverInfo, direction: AMPopTipDirection.up, maxWidth: 200, in: uiViewController?.view, fromFrame: self.moverInfoButton.frame)
    }
    
}
