//
//  OrderWebService.swift
//  business
//
//  Created by praveen kumar on 07/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class OrderWebService:WebServiceDelegate{
    var viewController:UIViewController?
    var showActivity = 1
    
    func processWebServicePtoError(_ dic:NSDictionary,identity: String){
        Constant.log("Enter "+identity,fileName: #file, functionName: #function, atLine: #line)
        print(dic)
        if (dic["pto"] == nil){
            if identity == "gccl" {
                
            }
            else{
                if identity == "oc" {
                    let vController = (viewController as! NewDeliveryViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//oc finish
                    
                    //oc begin
                else if identity == "rlo" {
                    let vController = (viewController as! NewDeliveryViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//oc finish
                    
                    //oc begin
                else if identity == "spp" {
                    let vController = (viewController as! MenuController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                                vController.personImage.image = UIImage(named: "person")
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//oc finish
                    
                    
                    //oc begin
                else if identity == "memr" {
                    let moneyViewController = (viewController as! MoneyViewController)
                    
                    //show loading popup
                    //Constant.showActivityIndicatory(moneyViewController.view,actInd: moneyViewController.actInd,loadingView: moneyViewController.loadingView,container: moneyViewController.container)
                    
                    //call web service
                    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                        let c = Constant()
                        c.webServiceDelegate=self
                        let dictionary: [String:String] = [
                            "key" : LoginModel.sessionkey,
                            "sessionkey" : LoginModel.sessionkey,
                            "startdate" : dic["startdate"] as! String,
                            "enddate" : dic["enddate"] as! String,
                            "medium" : String(3)
                        ]
                        
                        c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.memr as NSString,identity: identity as NSString)
                    });
                }//memr finish
                    
                    
                    //oc begin
                else if identity == "prmt" {
                    let vController = (viewController as! PastOrdersViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//prmt finish
                    
                    
                    //gti begin client token
                else if identity == "gti" {
                   
                    
                    //call web service
                    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                        let c = Constant()
                        c.webServiceDelegate=self
                        let dictionary: [String:String] = [
                            "key" : LoginModel.sessionkey,
                            "sessionkey" : LoginModel.sessionkey,
                            "medium" : String(3)
                        ]
                        
                        c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.gti as NSString,identity: identity as NSString)
                    });
                }//gti finish
                    
                    //icc begin provide nonce and insert credit card
                else if identity == "icc" {
                    let vController = (viewController as! AddCreditCardViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//gti finish
                    
                    //update corporate profile
                else if identity == "ump" {
                    let vController = (viewController as! UpdateProfileViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//gti finish
                    
                    //update corporate profile
                else if identity == "ca" {
                    let vController = (viewController as! ChangeAddressViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//gti finish
                    
                    //update corporate profile
                else if identity == "ccv" {
                    let vController = (viewController as! DeliveryTypeViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//gti finish
                    
                    //update corporate profile
                else if identity == "fct" {
                    
                    let deliveryTypeViewController = (viewController as! OrderSummaryTableViewController)
                    if (showActivity == 1) {
                        //show loading popup
                        Constant.showActivityIndicatory(deliveryTypeViewController.view,actInd: deliveryTypeViewController.actInd,loadingView: deliveryTypeViewController.loadingView,container: deliveryTypeViewController.container)
                    }
                    //call web service
                    DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                        let c = Constant()
                        c.webServiceDelegate=self
                        let dictionary: [String:String] = [
                            "key" : LoginModel.sessionkey,
                            "sessionkey" : LoginModel.sessionkey,
                            "medium" : String(3)
                        ]
                        
                        c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.fct as NSString,identity: identity as NSString)
                    });
                }//gti finish
                    
                    
                    //update corporate profile
                else if identity == "mqg" {
                    
                    let vController = (viewController as! CustomerSupportViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "mqs" {
                    
                    let vController = (viewController as! CustomerSupportViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    //update corporate profile
                else if identity == "uma" {
                    
                    let vController = (viewController as! NewDeliveryViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "uoa" {
                    
                    let vController = (viewController as! ChangeAddressViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "dco" {
                    
                    
                    let vController = (viewController as! NewDeliveryViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "aco" {
                    
                    let vController = (viewController as! RequestViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "cp" {
                    
                    let vController = (viewController as! RequestViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "fco" {
                    
                    let vController = (viewController as! RequestViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    
                    //update corporate profile
                else if identity == "av" {
                    
                    let vController = (viewController as! AddVehicleViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "dv" {
                    
                    let vController = (viewController as! VehicleViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "cv" {
                    
                    let vController = (viewController as! VehicleViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "gva" {
                    
                    let vController = (viewController as! VehicleViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    
                    //update corporate profile
                else if identity == "cco" {
                    
                    let vController = (viewController as! RequestViewController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
                    //update corporate profile
                else if identity == "cms" {
                    
                    let vController = (viewController as! MenuController)
                    
                    if let errorString = dic["error"] as? String {
                        DispatchQueue.main.async(execute: {
                            vController.switchOff()
                            if self.showActivity == 1 {
                                
                                vController.stopActivityIndicator()
                            }
                            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                            let confirmAction = UIAlertAction(
                            title: "OK", style: UIAlertActionStyle.default) { (action) in
                                //self.createPopUp(dic)
                            }
                            alertController.addAction(confirmAction)
                            vController.present(alertController, animated: true, completion: nil)
                        });
                    }
                }//sdc finish
            }
        }
        else{
            let pto = dic["pto"] as! String
            if pto == "gccl" {
                
            }
            else{
                
            }
        }
    }//processWebServicePtoErro finish
    
    func processWebServicePto(_ dic:NSDictionary,identity: String){
        Constant.log("Enter "+identity,fileName: #file, functionName: #function, atLine: #line)
        print(dic)
        if (dic["pto"] == nil){
            if identity == "memr" {
                let moneyViewController = viewController as! MoneyViewController
                if let report = dic["report"] as? [NSDictionary] {
                    var months = [String]()
                    var fare = [Double]()
                    var todayEarning : Double = 0.0
                    var weekEarning : Double = 0.0
                    var monthEarning : Double = 0.0
                    for i in 0..<report.count {
                        if (i==0){
                            todayEarning = Double(report[i]["cnt"] as! String)!
                        }
                        else if (i==1){
                            weekEarning = Double(report[i]["cnt"] as! String)!
                        }
                        else if (i==2){
                            monthEarning = Double(report[i]["cnt"] as! String)!
                        }
                        else{
                            months.append(report[i]["date"] as! String)
                            fare.append(Double(report[i]["cnt"] as! String)!)
                        }
                    }//report added
                    //moneyViewController.reloadResults(months, fare: fare, todayEarning: todayEarning, weekEarning: weekEarning, monthEarning: monthEarning)
                }
                //moneyViewController.stopActivityIndicator()

            }
                
            else if identity == "aco" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let requestViewController = viewController as! RequestViewController
                if self.showActivity == 1 {
                  //  requestViewController.stopActivityIndicator()
                }
                
                requestViewController.addMoverAccept(dic as! Dictionary<String, AnyObject>)
            }
            else if identity == "cco" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let requestViewController = viewController as! RequestViewController
                if self.showActivity == 1 {
                    //requestViewController.stopActivityIndicator()
                }
                
                requestViewController.reloadDataWithArrayWhenOrderCancelled()
            }
            else if identity == "cp" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let requestViewController = viewController as! RequestViewController
                if self.showActivity == 1 {
                    requestViewController.stopActivityIndicator()
                }
                
                requestViewController.processNextOrder()
            }
            else if identity == "fco" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let requestViewController = viewController as! RequestViewController
                if self.showActivity == 1 {
                    //requestViewController.stopActivityIndicator()
                }
                if requestViewController.isFinal == 1{
                    requestViewController.normalRequest()
                }
                else{
                    requestViewController.processNextOrder()
                }
            }
            else if identity == "gva" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let vehicleViewController = viewController as! VehicleViewController
                
                vehicleViewController.reloadDataWithArray(dic["data"] as! [Dictionary<String,AnyObject>])
                if self.showActivity == 1 {
                    vehicleViewController.stopActivityIndicator()
                }
            }
            else if identity == "cms" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let vehicleViewController = viewController as! MenuController
                
                if self.showActivity == 1 {
                    vehicleViewController.stopActivityIndicator()
                }
            }
            else if identity == "uml" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let requestViewController = viewController as! RequestViewController
                if self.showActivity == 1 {
                    requestViewController.stopActivityIndicator()
                }
            }
            else if identity == "gti" {
                //no need to stop activity indicator
                
                if let data = dic["data"] as? String {
                    let addCreditCardViewController = viewController as! AddCreditCardViewController
                    addCreditCardViewController.addClientToken(data)
                }
            }
                
            else if identity == "icc" {
                //no need to stop activity indicator
                
                let addCreditCardViewController = viewController as! AddCreditCardViewController
                addCreditCardViewController.stopActivityIndicator()
                addCreditCardViewController.closeMe();
                
            }
            else if identity == "ump" {
                //no need to stop activity indicator
                
                let updateProfileViewController = viewController as! UpdateProfileViewController
                
                updateProfileViewController.stopActivityIndicator()
                Constant.showDialog(self.viewController!, message: (dic["message"] as! String), title: "Message", completionHandler: nil)
                
            }
            else if identity == "ca" {
                //no need to stop activity indicator
                
                let changeAddressViewController = viewController as! ChangeAddressViewController
                
                changeAddressViewController.stopActivityIndicator()
                
                if self.viewController != nil {
                    OrderWebService.storyboardHandler(self.viewController!,pto: "changeaddress")
                }
            }
            else if identity == "ccv" {
                let deliveryTypeViewController = viewController as! DeliveryTypeViewController
                
                deliveryTypeViewController.stopActivityIndicator()
                deliveryTypeViewController.showNext()
            }
            else if identity == "oc" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let deliveryTypeViewController = viewController as! NewDeliveryViewController
                if deliveryTypeViewController.deliveryMode == Constant.LATER{
                    deliveryTypeViewController.stopActivityIndicator()
                    OrderWebService.storyboardHandler(deliveryTypeViewController, pto: "currentorders")
                }
            }
            else if identity == "rlo" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let deliveryTypeViewController = viewController as! NewDeliveryViewController
                if deliveryTypeViewController.deliveryMode == Constant.LATER{
                    deliveryTypeViewController.stopActivityIndicator()
                    OrderWebService.storyboardHandler(deliveryTypeViewController, pto: "currentorders")
                }
            }
            else if identity == "fct" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let orderSummaryTableViewController = viewController as! OrderSummaryTableViewController
                if self.showActivity == 1 {
                    orderSummaryTableViewController.stopActivityIndicator()
                }
                if let orders = dic["orders"] as? [Dictionary<String, AnyObject>]{
                    orderSummaryTableViewController.reloadDataWithArray(orders)
                }
            }
            else if identity == "prmt" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let pastOrdersViewController = viewController as! PastOrdersViewController
                if self.showActivity == 1 {
                    pastOrdersViewController.stopActivityIndicator()
                }
                if let orders = dic["orders"] as? [NSDictionary]{
                    
                    if let circles = dic["circles"] as? [NSDictionary]{
                        pastOrdersViewController.reloadDataWithArray(orders,circles: circles)
                    }
                }
            }
            else if identity == "ump" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let updateProfileViewController = viewController as! UpdateProfileViewController
                if self.showActivity == 1 {
                    updateProfileViewController.stopActivityIndicator()
                }
                
                DispatchQueue.main.async(execute: {
                    let messageString = dic["message"] as! String
                    let alertController = UIAlertController(title: "Message", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        //self.createPopUp(dic)
                    }
                    alertController.addAction(confirmAction)
                    updateProfileViewController.present(alertController, animated: true, completion: nil)
                });
            }
            else if identity == "mqg" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let customerSupportViewController = viewController as! CustomerSupportViewController
                if self.showActivity == 1 {
                    customerSupportViewController.stopActivityIndicator()
                }
                if let data = dic["data"] as? [NSDictionary]{
                    
                    customerSupportViewController.reloadDataWithArray(data)
                    
                }
            }
            else if identity == "mqs" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let customerSupportViewController = viewController as! CustomerSupportViewController
                if self.showActivity == 1 {
                    customerSupportViewController.stopActivityIndicator()
                }
                DispatchQueue.main.async(execute: {
                    let messageString = dic["message"] as! String
                    let alertController = UIAlertController(title: "Message", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        customerSupportViewController.textField.text! = ""
                        customerSupportViewController.answer.text! = ""
                    }
                    alertController.addAction(confirmAction)
                    customerSupportViewController.present(alertController, animated: true, completion: nil)
                });
            }
            else if identity == "uma" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let newDeliveryViewController = viewController as! NewDeliveryViewController
                if self.showActivity == 1 {
                    newDeliveryViewController.stopActivityIndicator()
                }
                if CheckCurrentController.newDeliveryViewController() == true {
                    if let data = dic["data"] as? [NSDictionary] {
                        newDeliveryViewController.addMarkers(data)
                    }
                }
            }
            else if identity == "uoa" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let changeAddressViewController = viewController as! ChangeAddressViewController
                if self.showActivity == 1 {
                    changeAddressViewController.stopActivityIndicator()
                }
                DispatchQueue.main.async(execute: {
                    let messageString = "Address has been updated successfully"
                    let alertController = UIAlertController(title: "Message", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        changeAddressViewController.dismissMe()
                    }
                    alertController.addAction(confirmAction)
                    changeAddressViewController.present(alertController, animated: true, completion: nil)
                });
            }
            else if identity == "av" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let orderSummaryTableViewController = viewController as! AddVehicleViewController
                if self.showActivity == 1 {
                    orderSummaryTableViewController.stopActivityIndicator()
                }
                DispatchQueue.main.async(execute: {
                    let messageString = String(describing: dic["message"]!)
                    let alertController = UIAlertController(title: "Message", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        //changeAddressViewController.dismissMe()
                        orderSummaryTableViewController.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(confirmAction)
                    orderSummaryTableViewController.present(alertController, animated: true, completion: nil)
                });
            }
            else if identity == "dv" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let orderSummaryTableViewController = viewController as! VehicleViewController
                if self.showActivity == 1 {
                    orderSummaryTableViewController.stopActivityIndicator()
                }
                DispatchQueue.main.async(execute: {
                    let messageString = "Vehicle is deleted successfully"
                    let alertController = UIAlertController(title: "Message", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        //changeAddressViewController.dismissMe()
                    }
                    alertController.addAction(confirmAction)
                    orderSummaryTableViewController.present(alertController, animated: true, completion: nil)
                });
            }
            else if identity == "cv" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let orderSummaryTableViewController = viewController as! VehicleViewController
                if self.showActivity == 1 {
                    orderSummaryTableViewController.stopActivityIndicator()
                }
                orderSummaryTableViewController.setDefaultVehicle()
                DispatchQueue.main.async(execute: {
                    let messageString = "Vehicle is changed successfully"
                    let alertController = UIAlertController(title: "Message", message: messageString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        //changeAddressViewController.dismissMe()
                        OrderWebService.storyboardHandler(orderSummaryTableViewController, pto: "swrevealfirst")
                        //orderSummaryTableViewController.performSegueWithIdentifier("changerequestseague", sender: orderSummaryTableViewController)
                    }
                    alertController.addAction(confirmAction)
                    orderSummaryTableViewController.present(alertController, animated: true, completion: nil)
                });
            }
            else if identity == "gva" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let orderSummaryTableViewController = viewController as! VehicleViewController
                if self.showActivity == 1 {
                    orderSummaryTableViewController.stopActivityIndicator()
                }
                orderSummaryTableViewController.reloadDataWithArray(dic["data"] as! [Dictionary<String,AnyObject>])
            }
            else if identity == "spp" {
                //Notification will stop the work
                //OrderWebService.storyboardHandler(self.viewController!,pto: "currentorders")
                let orderSummaryTableViewController = viewController as! MenuController
                if self.showActivity == 1 {
                    orderSummaryTableViewController.stopActivityIndicator()
                }
                LoginModel.imageurl = dic["url"] as! String
            }
        }
        else{
            let pto = dic["pto"] as! String
            if pto ==  "oc" {
                
            }
            else if pto ==  "dcc" {
                OrderWebService.storyboardHandler(self.viewController!,pto: "cardshow")
            }
            else if pto ==  "rlo" {
                
            }
            else if pto ==  "ccv" {
                if self.viewController != nil{
                    OrderWebService.storyboardHandler(self.viewController!,pto: "cardverify")
                }
            }
            else{
                //self.createPopUp(dic)
            }
        }
       
    }
    
    func processWebService(_ data: Data?, response: URLResponse?, error: Error?,identity: NSString){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        if data != nil {
            print(String(data:data!, encoding:.utf8))
        }
        
        //stopActivityIndicator()
        
        // 1: Check HTTP Response for successful GET request
        guard let httpResponse = response as? HTTPURLResponse, let _ = data
            else {
                print("error: not a valid http response")
                let errorString = "Internet not working"
                //let newDeliveryViewController = (viewController as! ExtraViewController)
                DispatchQueue.main.async(execute: {
                    //newDeliveryViewController.stopActivityIndicator()
                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        //exit(0)
                    }
                    alertController.addAction(confirmAction)
                    self.viewController!.present(alertController, animated: true, completion: nil)
                });
                return
        }
        print("processWebService Serializing data")
        switch (httpResponse.statusCode)
        {
        case 200:
            
            //let response = NSString (data: receivedData, encoding: NSUTF8StringEncoding)
            
                do{
                    if data != nil {
                        let dic = try (JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary) as! Dictionary<String,AnyObject>
                        print(dic)
                        let status = dic["status"] as! String
                        print(status)
                        if status == "success" {
                           processWebServicePto(dic as NSDictionary,identity: identity as String)
                        }
                        else {
                           processWebServicePtoError(dic as NSDictionary,identity: identity as String)
                        }
                    }
                }//if data finsihh
                catch{
                    print("reg error response")
                    //Constant.internetNotWorking(viewController!)
                }
            
        default:
            print("save profile POST request got response \(httpResponse.statusCode)")
        }
    }
    
    func executeWebService(_ viewController: UIViewController,dic: NSDictionary,identity: String,showActivity:Int = 1){
        self.viewController = viewController
        //oc begin
        if identity == "oc" {
            let newDeliveryViewController = (viewController as! NewDeliveryViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(newDeliveryViewController.view,actInd: newDeliveryViewController.actInd,loadingView: newDeliveryViewController.loadingView,container: newDeliveryViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "address"  : dic["address"] as! String,
                    "name"  : dic["name"] as! String,
                    "email"  : dic["email"] as! String,
                    "mobileno"  : dic["mobileno"] as! String,
                    "deliverymode"  : String(Constant.NOW)
                ]
                
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.oc as NSString,identity: identity as NSString)
            });
        }//oc finish
            
            //oc begin
        else if identity == "rlo" {
            let newDeliveryViewController = (viewController as! NewDeliveryViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(newDeliveryViewController.view,actInd: newDeliveryViewController.actInd,loadingView: newDeliveryViewController.loadingView,container: newDeliveryViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "address"  : dic["address"] as! String,
                    "name"  : dic["name"] as! String,
                    "email"  : dic["email"] as! String,
                    "mobileno"  : dic["mobileno"] as! String,
                    "deliverymode"  : String(Constant.LATER)
                ]
                
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.rlo as NSString,identity: identity as NSString)
            });
        }//oc finish
        
            
            //oc begin
        else if identity == "memr" {
            let moneyViewController = (viewController as! MoneyViewController)
            
            //show loading popup
            //Constant.showActivityIndicatory(moneyViewController.view,actInd: moneyViewController.actInd,loadingView: moneyViewController.loadingView,container: moneyViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "startdate" : dic["startdate"] as! String,
                    "enddate" : dic["enddate"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.memr as NSString,identity: identity as NSString)
            });
        }//memr finish
        
            
            //oc begin
        else if identity == "prmt" {
            let pastOrdersViewController = (viewController as! PastOrdersViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(pastOrdersViewController.view,actInd: pastOrdersViewController.actInd,loadingView: pastOrdersViewController.loadingView,container: pastOrdersViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "limit" : String(LoginModel.PASTORDERLIMIT),
                    "pageno" : dic["pageno"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.prmt as NSString,identity: identity as NSString)
            });
        }//prmt finish
            
            
            //gti begin client token
        else if identity == "gti" {
            //let addCreditCardViewController = (viewController as! AddCreditCardViewController)
            
            //show loading popup
            //Constant.showActivityIndicatory(addCreditCardViewController.view,actInd: addCreditCardViewController.actInd,loadingView: addCreditCardViewController.loadingView,container: addCreditCardViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.gti as NSString,identity: identity as NSString)
            });
        }//gti finish
            
            //icc begin provide nonce and insert credit card
        else if identity == "icc" {
            let addCreditCardViewController = (viewController as! AddCreditCardViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(addCreditCardViewController.view,actInd: addCreditCardViewController.actInd,loadingView: addCreditCardViewController.loadingView,container: addCreditCardViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "payment_method_nonce" : dic["payment_method_nonce"] as! String,
                    "last" : dic["last"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.icc as NSString,identity: identity as NSString)
            });
        }//gti finish
            
            //update corporate profile
        else if identity == "ump" {
            let updateProfileViewController = (viewController as! UpdateProfileViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(updateProfileViewController.view,actInd: updateProfileViewController.actInd,loadingView: updateProfileViewController.loadingView,container: updateProfileViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "password" : dic["password"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.ump as NSString,identity: identity as NSString)
            });
        }//gti finish
            
            //update corporate profile
        else if identity == "ca" {
            let changeAddressViewController = (viewController as! ChangeAddressViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(changeAddressViewController.view,actInd: changeAddressViewController.actInd,loadingView: changeAddressViewController.loadingView,container: changeAddressViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "address"  : dic["address"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.ca as NSString,identity: identity as NSString)
            });
        }//gti finish
            
            //update corporate profile
        else if identity == "ccv" {
            let deliveryTypeViewController = (viewController as! DeliveryTypeViewController)
            
            //show loading popup
            Constant.showActivityIndicatory(deliveryTypeViewController.view,actInd: deliveryTypeViewController.actInd,loadingView: deliveryTypeViewController.loadingView,container: deliveryTypeViewController.container)
            
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.ccv as NSString,identity: identity as NSString)
            });
        }//gti finish
            
            //update corporate profile
        else if identity == "fct" {
            self.showActivity = showActivity
            let deliveryTypeViewController = (viewController as! OrderSummaryTableViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(deliveryTypeViewController.view,actInd: deliveryTypeViewController.actInd,loadingView: deliveryTypeViewController.loadingView,container: deliveryTypeViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.fct as NSString,identity: identity as NSString)
            });
        }//gti finish
            
         
            //update corporate profile
        else if identity == "mqg" {
            self.showActivity = showActivity
            let customerSupportViewController = (viewController as! CustomerSupportViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(customerSupportViewController.view,actInd: customerSupportViewController.actInd,loadingView: customerSupportViewController.loadingView,container: customerSupportViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.mqg as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "mqs" {
            self.showActivity = showActivity
            let customerSupportViewController = (viewController as! CustomerSupportViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(customerSupportViewController.view,actInd: customerSupportViewController.actInd,loadingView: customerSupportViewController.loadingView,container: customerSupportViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "id" : dic["id"] as! String,
                    "message" : dic["message"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.mqs as NSString,identity: identity as NSString)
            });
        }//sdc finish
            //update corporate profile
        else if identity == "uma" {
            self.showActivity = showActivity
            let newDeliveryViewController = (viewController as! NewDeliveryViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(newDeliveryViewController.view,actInd: newDeliveryViewController.actInd,loadingView: newDeliveryViewController.loadingView,container: newDeliveryViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.uma as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "uma" {
            self.showActivity = showActivity
            let newDeliveryViewController = (viewController as! NewDeliveryViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(newDeliveryViewController.view,actInd: newDeliveryViewController.actInd,loadingView: newDeliveryViewController.loadingView,container: newDeliveryViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.uma as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "uml" {
            self.showActivity = showActivity
            let customerSupportViewController = (viewController as! RequestViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(customerSupportViewController.view,actInd: customerSupportViewController.actInd,loadingView: customerSupportViewController.loadingView,container: customerSupportViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "orderid" : dic["orderid"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.uml as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "dco" {
            self.showActivity = showActivity
            let customerSupportViewController = (viewController as! CustomerSupportViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(customerSupportViewController.view,actInd: customerSupportViewController.actInd,loadingView: customerSupportViewController.loadingView,container: customerSupportViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "lat" : dic["lat"] as! String,
                    "lng" : dic["lng"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.uma as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "aco" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! RequestViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "orderid" : dic["orderid"] as! String,
                    "usertype" : dic["usertype"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.aco as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "cp" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! RequestViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "orderid" : dic["orderid"] as! String,
                    "medium" : String(3)
                ]
                print("cp ")
                print(dictionary)
                c.getPostWebServiceWithImageResponse(dictionary, urlString: RouteUrl.cp as NSString,identity: identity as NSString,image: requestViewController.deliveryImage!)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "fco" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! RequestViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "orderid" : dic["orderid"] as! String,
                    "miles" : dic["miles"] as! String,
                    "isfinal" : dic["isfinal"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceWithImageResponse(dictionary, urlString: RouteUrl.fco as NSString,identity: identity as NSString,image: requestViewController.deliveryImage!)
            });
        }//sdc finish
      
            
            //update corporate profile
        else if identity == "av" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! AddVehicleViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "name" : dic["name"] as! String,
                    "vehiclenumber" : dic["vehiclenumber"] as! String,
                    "vehicletypeid"  : dic["vehicletypeid"] as! String,
                    "modelyear" : dic["modelyear"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceWithImageResponse(dictionary, urlString: RouteUrl.av as NSString,identity: identity as NSString,image: requestViewController.imageControl.currentImage!)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "spp" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! MenuController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceWithImageResponse(dictionary, urlString: RouteUrl.spp as NSString,identity: identity as NSString,image: requestViewController.pickedImage!)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "dv" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! VehicleViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "vehicleid" : dic["vehicleid"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.dv as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "cv" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! VehicleViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "vehicleid" : dic["vehicleid"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.cv as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "gva" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! VehicleViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "pageno" : dic["pageno"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.gva as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "cco" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! RequestViewController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "orderid" : dic["orderid"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.cco as NSString,identity: identity as NSString)
            });
        }//sdc finish
            
            //update corporate profile
        else if identity == "cms" {
            self.showActivity = showActivity
            let requestViewController = (viewController as! MenuController)
            if (showActivity == 1) {
                //show loading popup
                Constant.showActivityIndicatory(requestViewController.view,actInd: requestViewController.actInd,loadingView: requestViewController.loadingView,container: requestViewController.container)
            }
            //call web service
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "key" : LoginModel.sessionkey,
                    "sessionkey" : LoginModel.sessionkey,
                    "state" : dic["state"] as! String,
                    "medium" : String(3)
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.cms as NSString,identity: identity as NSString)
            });
        }//sdc finish
        
        
    }//executeWebService finish
    
    static func storyboardHandler(_ viewController: UIViewController, pto: String){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        if pto == "swrevealfirst" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let deliveryViewController = 	storyboard.instantiateViewController(withIdentifier: "swrevealfirst") as! SWRevealViewController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(deliveryViewController, animated: true, completion: nil)
            }
        }
        if pto == "allvehicles" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let deliveryViewController = 	storyboard.instantiateViewController(withIdentifier: "allvehiclesview") as! VehicleViewController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(deliveryViewController, animated: true, completion: nil)
            }
        }
        else if pto == "changeaddress" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let changeAddressNavigationController = storyboard.instantiateViewController(withIdentifier: "changeaddress") as! ChangeAddressNavigationController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(changeAddressNavigationController, animated: true, completion: nil)
            }
        }
        else if pto == "requestnavigation" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let changeAddressNavigationController = storyboard.instantiateViewController(withIdentifier: "requestnavigation") as! RequestNavigationController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(changeAddressNavigationController, animated: true, completion: nil)
            }
        }
        else if pto == "deliverynavigation" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let newDeliveryNavigationController = 	storyboard.instantiateViewController(withIdentifier: "deliverynavigation") as! NewDeliveryNavigationController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(newDeliveryNavigationController, animated: true, completion: nil)
            }
        }
        else if pto == "currentorders" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let newDeliveryNavigationController = 	storyboard.instantiateViewController(withIdentifier: "currentorders") as! OrderSummaryTableViewController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(newDeliveryNavigationController, animated: true, completion: nil)
            }
        }
        else if pto == "startentry" {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let newDeliveryNavigationController = 	storyboard.instantiateViewController(withIdentifier: "startentry") as! StartViewController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                viewController.present(newDeliveryNavigationController, animated: true, completion: nil)
            }
        }
    }
    
    
}
