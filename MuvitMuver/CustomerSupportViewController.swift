//
//  CustomerSupportViewController.swift
//  business
//
//  Created by praveen kumar on 11/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class CustomerSupportViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    var data=[NSDictionary]()
    var picker = UIPickerView()
    var selectedRow = -1
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var answer: UITextView!
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Constant.setCurrentController(self)
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        answer!.layer.borderWidth = 1
        answer!.layer.borderColor = UIColor.gray.cgColor
        picker.delegate = self
        picker.dataSource = self
        textField.inputView = picker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(CustomerSupportViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        answer.inputAccessoryView = toolBar
        
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [
            :
        ]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "mqg")
    }
    
    func doneClick() {
        textField.resignFirstResponder()
        answer.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    @IBAction func sendPressed(_ sender: AnyObject) {
        if selectedRow != -1 {
            
            let orderWebService = OrderWebService()
            let dictionary: [String:String] = [
                "id" : data[selectedRow]["id"] as! String,
                 "message" : answer.text!
            ]
            orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "mqs")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        textField.text = data[row]["name"] as? String
        selectedRow = row
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]["name"] as? String
    }
    func reloadDataWithArray(_ data:[NSDictionary]){
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        self.data = data
    }
}
