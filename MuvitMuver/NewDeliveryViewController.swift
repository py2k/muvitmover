//
//  NewDeliveryViewController.swift
//  business
//
//  Created by praveen kumar on 06/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//orderid, status, address, mover name, mover number, is delivered

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftValidator

class NewDeliveryViewController: UIViewController,UISearchBarDelegate,LocateOnTheMap,CLLocationManagerDelegate, UITextFieldDelegate,ValidationDelegate {
    var googleMapsView: GMSMapView!
    @IBOutlet weak var googleMapsContainer: UIView!
    var searchResultsController: SearchResultsController!
    var searchController: UISearchController!
    var resultsArray = [String]()
    let locationManager=CLLocationManager()
    //@IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var mobilenoTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    let validator = Validator()
    var presentWindow : UIWindow?
    var deliveryMode = Constant.NOW
    var searchAddress:String = ""
    var lat:String = ""
    var lng:String = ""
    let gmo = GoogleMapsOperation()
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func setDeliveryModes(_ deliveryMode: Int){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        self.deliveryMode = deliveryMode
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    override func viewDidLoad() { 
        super.viewDidLoad()
        Constant.setCurrentController(self)
        //Constant.newDeliveryViewController = self
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        else{
            print("reveal view controller is not present")
        }
        // Do any additional setup after loading the view, typically from a nib.
        self.locationManager.delegate=self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        presentWindow = UIApplication.shared.keyWindow
        
        
        UIView.hr_setToastThemeColor(color: Constant.hexStringToUIColor("225378",alpha: 1)  )
        
        validator.registerField(nameTextField, errorLabel: nil, rules: [RequiredRule()])
        validator.registerField(emailTextField, errorLabel: nil, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
        
        validator.registerField(mobilenoTextField, errorLabel: nil, rules: [RequiredRule(), MinLengthRule(length: 14), MaxLengthRule(length: 14)])
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (_, error) in errors {
            //    if let field = field as? UITextField {
            //field.layer.borderColor = UIColor.redColor().CGColor
            //field.layer.borderWidth = 1.0
            //      }
            if error.errorLabel == nil {
                //presentWindow.makeToastActivityWithMessage(message: error.errorMessage)
                presentWindow!.makeToast(message: error.errorMessage, duration: 3, position: HRToastPositionCenter as AnyObject, title: "Error")
            }
            else{
                error.errorLabel?.text = error.errorMessage // works if you added labels
                error.errorLabel?.isHidden = false
            }
        }
    }
    
    func validationSuccessful() {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        if (self.searchAddress != ""){
            let newString = self.mobilenoTextField.text!.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
            let orderWebService = OrderWebService()
            let dic: [String:String] = [
                "lat" : self.lat,
                "lng" : self.lng,
                "address"  : Constant.stringByAddingPercentEncodingForFormData(self.searchAddress)!,
                "name"  : self.nameTextField.text!,
                "email"  : self.emailTextField.text!,
                "mobileno":  newString
            ]
            if deliveryMode == Constant.NOW{
                orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "oc")
            }
            else{
                orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "rlo")
            }
        }
        else{
             DispatchQueue.main.async(execute: {
            let errorString = "Enter address"
            let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
            let confirmAction = UIAlertAction(
            title: "OK", style: UIAlertActionStyle.default) { (action) in
                
            }
            alertController.addAction(confirmAction)
                
                self.present(alertController, animated: true, completion: nil)
               });
        }
    }
    
    @IBAction func setDropOffButtonClicked(_ sender: AnyObject) {
        validator.validate(self)
    }
    
    func checkEnglishPhoneNumberFormat(_ string: String?, str: String?) -> Bool{
       
        if string == "" { //BackSpace
            
            return true
            
        }else if str!.characters.count < 3{
            
            if str!.characters.count == 1{
                
                mobilenoTextField.text = "("
            }
            
        }else if str!.characters.count == 5{
            
            mobilenoTextField.text = mobilenoTextField.text! + ") "
            
        }else if str!.characters.count == 10{
            
            mobilenoTextField.text = mobilenoTextField.text! + "-"
            
        }else if str!.characters.count > 14{
            
            return false
        }
        
        return true
    }
    
    // Move the text field in a pretty animation!
    func moveTextField(_ textField: UITextField, moveDistance: Int, up: Bool) {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        if textField == mobilenoTextField {
            let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            
            return checkEnglishPhoneNumberFormat(string, str: str)
        }
        return true;
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        //scrollView.setContentOffset(CGPointMake(0, 250), animated: true)
        moveTextField(textField, moveDistance: -250, up: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        textField.resignFirstResponder()
        return true;
    }
    
    // Finish Editing The Text Field
    func textFieldDidEndEditing(_ textField: UITextField) {
        moveTextField(textField, moveDistance: -250, up: false)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        //scrollView.setContentOffset(CGPointMake(0, 0), animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        //Constant.newDeliveryViewController = self
        Constant.currentViewController = self
       
        searchResultsController = SearchResultsController()
        searchResultsController.delegate = self
        searchController = UISearchController(searchResultsController:searchResultsController)
        //searchController.searchResultsController = searchResultsController
        searchController.searchBar.sizeToFit()
        searchController.searchBar.placeholder = "Search for places"
        navigationItem.titleView = searchController.searchBar
        searchController.searchBar.delegate=self
        
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = true
        definesPresentationContext = true
        
        
        if self.googleMapsView == nil {
            self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
            self.googleMapsView.settings.myLocationButton=true
            self.googleMapsView.isMyLocationEnabled=true
            self.view.insertSubview(self.googleMapsView, at: 0)
        }
        TimerHandle.startTimer()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let lat = self.locationManager.location?.coordinate.latitude
        let lon = self.locationManager.location?.coordinate.longitude
        if lat != nil && lon != nil {
            LoginModel.currentLat = lat!
            LoginModel.currentLng = lon!
        }
        locateWithLongitude(lon!, andLatitude: lat!, andTitle: "New Location")
        self.locationManager.stopUpdatingLocation()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        print(searchText)
        //let searchText = sender.attributedText!.string
        let placesClient = GMSPlacesClient()
        placesClient.autocompleteQuery(searchText, bounds: nil, filter: nil){ (results,error: Error?) -> Void in
            self.resultsArray.removeAll()
            if results == nil{
                return
            }
            //print(results)
            for result in results! {
                //print(result.attributedFullText.string)
                self.resultsArray.append(result.attributedFullText.string)
                
            }
            print(self.resultsArray)
            self.searchResultsController.reloadDataWithArray(self.resultsArray)
        }
        
    }
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        DispatchQueue.main.async{() -> Void in
            let position = CLLocationCoordinate2DMake(lat, lon)
            let marker = GMSMarker(position:position)
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 14)
            
            if self.googleMapsView == nil {
                self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
                self.googleMapsView.settings.myLocationButton=true
                self.googleMapsView.isMyLocationEnabled=true
                self.view.insertSubview(self.googleMapsView, at: 0)
            }
            self.googleMapsView.camera=camera
            
            marker.title=title
            marker.map=self.googleMapsView
            
            self.lat = String(lat)
            self.lng = String(lon)
            self.searchAddress = title
            
        }
    }
    
    func addMarkers(_ data:[NSDictionary]){
        gmo.googleMapsView = self.googleMapsView
        
        gmo.addMarkersFromWebService(data)
    }
    
}

