//
//  MoneyViewController.swift
//  SidebarMenu
//


import UIKit
//import Charts


class MoneyViewController: UIViewController, UITextFieldDelegate {
   /*
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet weak var barChartView: BarChartView!
    
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    var months: [String]!
    var fare : [Double]!
    var todayEarning : Double = 0.0
    var weekEarning : Double = 0.0
    var monthEarning : Double = 0.0
    var pastStartDate = "2016-09-01"
    var pastEndDate = "2016-09-15"
    var nowStartDate = "2016-09-01"
    var nowEndDate = "2016-09-15"

    @IBOutlet weak var todayEarningLabel: UILabel!
    
    @IBOutlet weak var weekEarningLabel: UILabel!
    @IBOutlet weak var monthEarningLabel: UILabel!
    
    
    func doneClick() {
        startDateTextField.resignFirstResponder()
        endDateTextField.resignFirstResponder()
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == startDateTextField {
            let dataPicker1 = UIDatePicker()
            dataPicker1.datePickerMode = UIDatePickerMode.date
            startDateTextField.inputView = dataPicker1
            dataPicker1.addTarget(self, action: #selector(self.datePickerChanged), for: UIControlEvents.valueChanged)
            
        }
        else if textField == endDateTextField {
            let dataPicker2 = UIDatePicker()
            dataPicker2.datePickerMode = UIDatePickerMode.date
            endDateTextField.inputView = dataPicker2
            dataPicker2.addTarget(self, action: #selector(self.datePickerChanged1), for: UIControlEvents.valueChanged)
        }
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(MoneyViewController.doneClick))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }
    @IBAction func hideKeyboard(_ sender: AnyObject) {
        
        startDateTextField.resignFirstResponder()
        endDateTextField.resignFirstResponder()
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    func datePickerChanged(_ sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.dateFormat = "yyyy-MM-dd"
        nowStartDate = formatter.string(from: sender.date)
        startDateTextField.text = formatter.string(from: sender.date)
        checkDataChanged()
    }
    func datePickerChanged1(_ sender: UIDatePicker){
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.dateFormat = "yyyy-MM-dd"
        nowEndDate = formatter.string(from: sender.date)
        endDateTextField.text = formatter.string(from: sender.date)
        checkDataChanged()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func checkDataChanged(){
        if (pastStartDate != nowStartDate || pastEndDate != nowEndDate) {
            callWebService()
        }
        
    }
    func callWebService(){
        
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [
            "startdate" : nowStartDate,
            "enddate" : nowEndDate
        ]
        orderWebService.executeWebService(self, dic: dictionary, identity: "memr")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        
        //months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        //fare = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        
        //setChart(months, values: fare)
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        callWebService()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    func setChart(_ dataPoints: [String], values: [Double]) {
        barChartView.noDataText = "You need to provide data for the chart."
        
        var dataEntries: [BarChartDataEntry] = []
         barChartView.noDataText = "No Entry Found."
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(value: values[i], xIndex:  i)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet( yVals: dataEntries, label: "Money Earned")
        let chartData = BarChartData(xVals: months,dataSet: chartDataSet)
        barChartView.data = chartData
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setMonthsAndFare(_ months: [String], fare: [Double], todayEarning: Double, weekEarning: Double, monthEarning: Double){
        self.months = months
        self.fare = fare
    }
    
    func reloadResults(_ months: [String], fare: [Double], todayEarning: Double, weekEarning: Double, monthEarning: Double){
        
        todayEarningLabel.text = String(todayEarning)
        weekEarningLabel.text = String(weekEarning)
        monthEarningLabel.text = String(monthEarning)
        
        setMonthsAndFare(months,fare: fare,todayEarning: todayEarning,weekEarning: weekEarning,monthEarning: monthEarning)
        setChart(months, values: fare)
    }
 */
}
