//
//  UpdateProfileViewController.swift
//  SidebarMenu
//
//  Created by Kapil Rathore on 28/08/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController {
    @IBOutlet weak var menuButton:UIBarButtonItem!
    
    @IBOutlet weak var passwordTextField: UITextField!
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    @IBAction func hideKeyboard(_ sender: AnyObject) {
        passwordTextField.resignFirstResponder()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    @IBAction func updateProfilePressed(_ sender: AnyObject) {
       
        let dic: [String:String] = [
            "password" : passwordTextField.text!
        ]
        let orderWebService = OrderWebService()
        orderWebService.executeWebService(self,dic: dic as NSDictionary,identity: "ump")
    
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
}
