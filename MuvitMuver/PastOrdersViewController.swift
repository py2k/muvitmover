//
//  PastOrdersViewController.swift
//  SidebarMenu
//
//  Created by Kapil Rathore on 28/08/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class PastOrdersViewController: UITableViewController {
    @IBOutlet weak var menuButton:UIBarButtonItem!
    var googleMapsView: GMSMapView?
    
    @IBOutlet weak var googleMapsContainer: UIView!
    
    var orders = [NSDictionary]()
    var circles = [NSDictionary]()
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        tableView.separatorStyle = UITableViewCellSeparatorStyle.singleLine
        //tableView.separatorColor = UIColor(patternImage: UIImage(named: "YOUR_IMAGE_NAME")!)
        
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [
            "pageno" : "1"
        ]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "prmt")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillData(_ orders:[NSDictionary], circles:[NSDictionary]){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        self.orders = orders
        self.circles = circles
    }
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    
    func loadMaps(){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        var lat = 26.9211992
        var lng = 75.8185761
        DispatchQueue.main.async(execute: {
            self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
            self.view.addSubview(self.googleMapsView!)
            if self.circles.count > 0 {
                lat = (self.circles[0]["lat"] as! NSString).doubleValue
                lng = (self.circles[0]["lng"] as! NSString).doubleValue
            }
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lng, zoom: 12)
            self.googleMapsView!.camera=camera
            
            let gmo = GoogleMapsOperation()
            gmo.googleMapsView = self.googleMapsView
            gmo.addAllCircle(self.circles)
        })
    }
    
    //Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        return self.orders.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let cell=tableView.dequeueReusableCell(withIdentifier: "pastorderscell", for: indexPath) as! CustomPastOrderTableViewCell
       
            if self.orders[(indexPath as NSIndexPath).row]["totalamount"] != nil {
                cell.amount.text="$ " + ((self.orders[(indexPath as NSIndexPath).row]["totalamount"] as! String))
            }
            if self.orders[(indexPath as NSIndexPath).row]["starttime"] != nil {
                cell.tripDate.text=(self.orders[(indexPath as NSIndexPath).row]["starttime"] as! String)
            }
            if self.orders[(indexPath as NSIndexPath).row]["vehiclename"] != nil {
                cell.vehicleName.text=(self.orders[(indexPath as NSIndexPath).row]["vehiclename"] as! String)
            }
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        DispatchQueue.main.async{
            let storyboard =  UIStoryboard(name: "Main", bundle: nil)
            let rideInfoViewController = 	storyboard.instantiateViewController(withIdentifier: "rideinfo") as! RideInfoViewController
            
            //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
            self.present(rideInfoViewController, animated: true, completion: nil)
            rideInfoViewController.setData(self.orders[(indexPath as NSIndexPath).row])
            
           }
        
        }
    
        func reloadDataWithArray(_ orders:[NSDictionary],circles:[NSDictionary]){
            
            Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
            self.orders = orders
            self.circles = circles
            self.loadMaps()
            self.tableView.reloadData()
        }
}
