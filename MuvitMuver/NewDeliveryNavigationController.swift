//
//  NewDeliveryNavigationController.swift
//  business
//
//  Created by praveen kumar on 08/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class NewDeliveryNavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    func setDeliveryMode(_ deliveryMode:Int){
        
        let deliveryViewController = self.viewControllers[0] as! NewDeliveryViewController
        deliveryViewController.setDeliveryModes(deliveryMode)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
