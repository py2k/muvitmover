//
//  DeliveryTypeViewController.swift
//  business
//
//  Created by praveen kumar on 04/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class DeliveryTypeViewController: UIViewController {
    
    @IBOutlet weak var nowButton: UIButton!
    @IBOutlet weak var laterButton: BezierButtonDrawing!
    
    @IBOutlet weak var waitImageView: UIImageView!
    @IBOutlet weak var waitLabel: UILabel!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    var deliveryMode = Constant.NOW
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        //Here I'm masking the textView's layer with rectShape layer
        Constant.roundCorners([.topLeft, .bottomLeft], radius: 50,button: nowButton)
        Constant.roundCorners([.topLeft, .bottomLeft], radius: 50,button: laterButton)
        waitImageView.image = UIImage.gifWithName("loading-circles")
        waitLabel.text = LoginModel.deliverymodetime
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any!)
    {
        if (segue.identifier == "pushdelivery")
        {
            let deliveryViewController = segue.destination as! NewDeliveryNavigationController
            deliveryViewController.setDeliveryMode(self.deliveryMode)
        }
    }
    
    func showNext(){
        DispatchQueue.main.async{
            
            if self.revealViewController() != nil {
//                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
//                let deliveryViewController = 	storyboard.instantiateViewControllerWithIdentifier("deliverynavigation") as! NewDeliveryNavigationController
//                
//                deliveryViewController.setDeliveryMode(self.deliveryMode)
//                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
//                self.presentViewController(deliveryViewController, animated: true, completion: nil)
                self.performSegue(withIdentifier: "pushdelivery", sender: self)
            }
        }
    }
    
    @IBAction func nowButtonClicked(_ sender: AnyObject) {
        let orderWebService = OrderWebService()
        let dic: [String:String] = [:]
        orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "ccv")
    }
    
    @IBAction func laterButtonClicked(_ sender: AnyObject) {
        self.deliveryMode = Constant.LATER
        let orderWebService = OrderWebService()
        let dic: [String:String] = [:]
        orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "ccv")
    }
    @IBOutlet weak var nowButtonClicked: UIButton!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
