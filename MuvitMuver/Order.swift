//
//  Order.swift
//
//
//  Created by praveen kumar on 07/09/16.
//
//

import Foundation
import CoreData


class Order: NSManagedObject {
    
    // Insert code here to add functionality to your managed object subclass
    
    
    static func saveOrder(_ orderid: Int, address: String,movername: String,movernumber: String,status: String,isdelivered: Int) -> String{
        let moc = Constant.getMoc()
        let entity = NSEntityDescription.insertNewObject(forEntityName: "Order", into: moc) as! Order
        entity.setValue(orderid, forKey: "orderid")
        entity.setValue(status, forKey: "status")
        entity.setValue(address, forKey: "status")
        entity.setValue(movername, forKey: "email")
        entity.setValue(movernumber, forKey: "password")
        entity.setValue(isdelivered, forKey: "mobileno")
        
        do{
            try moc.save()
            return "success"
        }
        catch{
            print("failure to save context : \(error)")
        }
        return "fail"
    }
    
    static func fetchOrder() -> Order? {
        deleteOrder()
        let moc = Constant.getMoc()
        let OrderFetch : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Order")
        
        do{
            let fetchedOrder = try moc.fetch(OrderFetch) as! [Order]
            if fetchedOrder.first == nil {
                return nil
            }
            else {
                print(fetchedOrder.first!.orderid!)
                return fetchedOrder.first!
            }
        }
        catch{
            print("Order not present")
            return nil
        }
    }
    
    static func deleteOrder() {
        let moc = Constant.getMoc()
        let OrderFetch : NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Order")
        
        do{
            let fetchedOrder = try moc.fetch(OrderFetch) as! [Order]
            var i=0
            if fetchedOrder.count > 0 {
                while i < fetchedOrder.count {
                    moc.delete(fetchedOrder[i])
                    i += 1
                }
                
                do{
                    try moc.save()
                    print("successfully saved Order")
                }
                catch{
                    print("failure to save context : \(error)")
                }
            }
            
        }
        catch{
            print("Order not present")
            //return nil
        }
    }
    static func updateAppId(_ appId: String) {
        let moc = Constant.getMoc()
        let OrderFetch: NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: "Order")
        
        do{
            let fetchedOrder = try moc.fetch(OrderFetch) as! [Order]
            if fetchedOrder.first == nil {
            }
            else {
            }
        }
        catch{
            print("Order not present")
        }
    }
    
}
