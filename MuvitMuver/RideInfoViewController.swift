//
//  RideInfoViewController.swift
//  business
//
//  Created by praveen kumar on 12/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import GoogleMaps

class RideInfoViewController: UIViewController {

    @IBOutlet weak var googleMapsContainer: UIView!
    var googleMapsView: GMSMapView?
    
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var startAddress: UILabel!
    @IBOutlet weak var endAddress: UILabel!
    @IBOutlet weak var endTime: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
       
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    func loadMap(_ order:NSDictionary){
        
        let fromlat = Double(order["fromlat"] as! String)
        let fromlng = Double(order["fromlng"]  as! String)
        let lat = Double(order["endlat"] as! String)
        let lng = Double(order["endlng"]  as! String)
        self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
        self.view.insertSubview(self.googleMapsView!, at: 0)
       
        let camera = GMSCameraPosition.camera(withLatitude: lat!, longitude: lng!, zoom: 12)
        self.googleMapsView!.camera = camera
        
        let origin = String(describing: fromlat) + "," + String(describing: fromlng)
        let destination = String(describing: lat) + "," + String(describing: lng)
        
        let gmo = GoogleMapsOperation()
        gmo.googleMapsView = self.googleMapsView
        
        gmo.getDirections(origin, destination: destination, waypoints: [], travelMode: GoogleMapsOperation.TravelModes.driving, completionHandler: nil)
    }

    func setData(_ order:NSDictionary){
        startTime.text! = order["starttime"] as! String
        startAddress.text! = order["fromaddress"] as! String
        endTime.text! = order["endtime"] as! String
        endAddress.text! = order["endaddress"] as! String
        totalAmount.text! = "$ "+(order["totalamount"] as! String)
        self.loadMap(order)
    }
    
    @IBAction func backButtonPressed(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
