//
//  User.swift
//  
//
//  Created by praveen kumar on 07/09/16.
//
//

import Foundation
import CoreData


class User: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    
    
    static func saveUser(_ firmname: String, officername: String,email: String,password: String,mobileno: String) -> String{
        let moc = Constant.getMoc()
        let entity = NSEntityDescription.insertNewObject(forEntityName: "User", into: moc) as! User
        entity.setValue(firmname, forKey: "firmname")
        entity.setValue(officername, forKey: "officername")
        entity.setValue(email, forKey: "email")
        entity.setValue(password, forKey: "password")
        entity.setValue(mobileno, forKey: "mobileno")
        
        do{
            try moc.save()
            return "success"
        }
        catch{
            print("failure to save context : \(error)")
        }
        return "fail"
    }
    
    static func fetchUser() -> User? {
        deleteUser()
        let moc = Constant.getMoc()
        let userFetch: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "User")
        
        do{
            let fetchedUser = try moc.fetch(userFetch) as! [User]
            if fetchedUser.first == nil {
                return nil
            }
            else {
                print(fetchedUser.first!.firmname!)
                return fetchedUser.first!
            }
        }
        catch{
            print("user not present")
            return nil
        }
    }
    
    static func deleteUser() {
        let moc = Constant.getMoc()
        let userFetch: NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: "User")
        
        do{
            let fetchedUser = try moc.fetch(userFetch) as! [User]
            var i=0
            if fetchedUser.count > 0 {
                while i < fetchedUser.count {
                    moc.delete(fetchedUser[i])
                    i += 1
                }
                
                do{
                    try moc.save()
                    print("successfully saved user")
                }
                catch{
                    print("failure to save context : \(error)")
                }
            }
            
        }
        catch{
            print("user not present")
            //return nil
        }
    }
    static func updateAppId(_ appId: String) {
        let moc = Constant.getMoc()
        let userFetch: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "User")
        
        do{
            let fetchedUser = try moc.fetch(userFetch) as! [User]
            if fetchedUser.first == nil {
            }
            else {
                print(fetchedUser.first!.firmname!)
                fetchedUser.first!.appid = appId
                do{
                    try moc.save()
                    print("successfully updated appid")
                }
                catch{
                    print("failure to save context : \(error)")
                }
            }
        }
        catch{
            print("user not present")
        }
    }

}
