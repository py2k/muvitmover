//
//  PopUpViewController.swift
//  PopUp
//
//  Created by Andrew Seeley on 6/06/2016.
//  Copyright © 2016 Seemu. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController,WebServiceDelegate {
    
    //@IBOutlet weak var msgTextView: UITextView!
    @IBOutlet weak var emailotpTextField: UITextField!
    @IBOutlet weak var emailotpSubmitButton: UIButton!
    @IBOutlet weak var emailotpLabel: UILabel!
    @IBOutlet weak var notreceived: UILabel!
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    var status:Int = 0
    var identity:String = ""
    var email:String = ""
    var mobileno:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        Constant.drawRectBorderButton(emailotpSubmitButton)
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        
        
        self.showAnimate()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Constant.drawBottomBorderTextField(emailotpTextField)
        //initPlot()
        self.view.isUserInteractionEnabled = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PopUpViewController.tapFunction(_:)))
        notreceived.addGestureRecognizer(gestureRecognizer)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    
    func tapFunction(_ gestureRecognizer: UIGestureRecognizer) {
        print("tap working")
        if self.identity == "ae" || self.identity == "age" {
            handlePtoButtonClick("age")
        }
        if self.identity == "am" || self.identity == "agm"{
            handlePtoButtonClick("agm")
        }
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
         }
    }
    func processRegistration(_ dic: NSDictionary){
        stopActivityIndicator()
        print("processRegistration dic")
        //print(dic)
        let identity = dic["pto"] as! String
        self.identity = identity
        if identity == "ae" {
            self.emailotpLabel.text="Enter the email verification code"
            self.emailotpLabel.isHidden = false
            self.emailotpTextField.isHidden = false
            self.emailotpSubmitButton.isHidden = false
            self.notreceived.isHidden = false
            
            
        }//ae finish
            
        else if identity == "am" {
            self.emailotpLabel.text="Enter the mobile verification code"
            self.emailotpLabel.isHidden = false
            self.emailotpTextField.isHidden = false
            self.emailotpTextField.text = ""
            self.emailotpSubmitButton.isHidden = false
            self.notreceived.isHidden = false
            
            
        }//am finish
            
        else if identity == "ad" {
            let disclaimerMessage = dic["tos"] as! String
            //self.dismissViewControllerAnimated(true, completion: {
            DispatchQueue.main.async{
                let storyboard =  UIStoryboard(name: "Main", bundle: nil)
                let disclaimerViewController = 	storyboard.instantiateViewController(withIdentifier: "disclaimer") as! DisclaimerNavigationController
                //self.parentViewController(disclaimerViewController, animated: true, completion: nil)
                self.present(disclaimerViewController, animated: true, completion: nil)
                disclaimerViewController.setEmail(self.email)
                disclaimerViewController.setTos(disclaimerMessage)
            }
            //})
            
        }//am finish
            
        else if identity == "approve" {
            let approveString = dic["approvemessage"] as! String
            //let errorString = dic["error"] as! String
            DispatchQueue.main.async(execute: {
                let alertController = UIAlertController(title: "Message", message: approveString, preferredStyle: UIAlertControllerStyle.alert)
                let confirmAction = UIAlertAction(
                title: "OK", style: UIAlertActionStyle.default) { (action) in
                    exit(0)
                }
                alertController.addAction(confirmAction)
                self.present(alertController, animated: true, completion: nil)
                
            });
            
            
        }//am finish
        
    }
    
    func processWebService(_ data: Data?, response: URLResponse?, error: Error?,identity: NSString){
        
        stopActivityIndicator()
        
        // 1: Check HTTP Response for successful GET request
        guard let httpResponse = response as? HTTPURLResponse, let _ = data
            else {
                print("error: not a valid http response")
                let errorString = "Internet not working"
                DispatchQueue.main.async(execute: {
                    self.stopActivityIndicator()
                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        exit(0)
                    }
                    alertController.addAction(confirmAction)
                    self.present(alertController, animated: true, completion: nil)
                });
                
                return
        }
        
        switch (httpResponse.statusCode)
        {
          case 200:
            do{
                if data != nil {
                     let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                    let status = dic["status"] as! String
                    print(status)
                    if dic["pto"] == nil {
                        
                    }
                    else{
                        if status == "success" {
                            self.processRegistration(dic)
                        }
                        else {
                            print("showdialog")
                            let errorString = dic["error"] as! String
                            DispatchQueue.main.async(execute: {
                                 self.stopActivityIndicator()
                                let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                                let confirmAction = UIAlertAction(
                                title: "OK", style: UIAlertActionStyle.default) { (action) in
                                    self.processRegistration(dic)
                                }
                                alertController.addAction(confirmAction)
                                self.present(alertController, animated: true, completion: nil)
                                
                            });
                           
                        }
                    }
                }
            }
            catch{
                Constant.internetNotWorking(self)
                print("reg error response")
            }
            
          default:
            print("save profile POST request got response \(httpResponse.statusCode)")
       }
    }
    
    @IBAction func hideKeyboard(_ sender: AnyObject) {
        emailotpTextField.resignFirstResponder()
    }
    
    func handlePtoButtonClick(_ identity:String){
        emailotpTextField.resignFirstResponder()
        self.identity = identity
        Constant.showActivityIndicatory(self.view,actInd: self.actInd,loadingView: self.loadingView,container: self.container)
        //what submit: email submit
        if identity == "ae" {
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "email" : self.email,
                    "code" : self.emailotpTextField.text!
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.ae as NSString,identity: identity as NSString)
            });
        }//identity email finished
        
        //what submit: email submit
        else if identity == "age" {
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "email" : self.email
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.age as NSString,identity: identity as NSString)
            });
        }//identity email finished
       
        //what submit: sms submit
        else if identity == "am" {
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "mobileno" : self.mobileno,
                    "email" : self.email,
                    "code" : self.emailotpTextField.text!
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.am as NSString,identity: identity as NSString)
            });
        }//identity email finished
                
            //what submit: sms submit
        else if identity == "agm" {
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "email" : self.email,
                    "mobileno" : self.mobileno
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.agm as NSString,identity: identity as NSString)
            });
        }//identity email finished
            
            //what submit: sms submit
        else if identity == "ad" {
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
                let dictionary: [String:String] = [
                    "email" : self.email
                ]
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.ad as NSString,identity: identity as NSString)
            });
        }//identity email finished
        
    }
    
    @IBAction func otpSubmit(_ sender: AnyObject) {
        //self.removeAnimate()
         Constant.showActivityIndicatory(self.view,actInd: self.actInd,loadingView: self.loadingView,container: self.container)
         self.handlePtoButtonClick(self.identity)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //@IBAction func closePopUp(sender: AnyObject) {
      //  self.removeAnimate()
        //self.view.removeFromSuperview()
    //}
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    
}
