//
//  DisclaimerNavigationController.swift
//  muv
//
//  Created by praveen kumar on 20/08/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class DisclaimerNavigationController: UINavigationController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        // Do any additional setup after loading the view, typically from a nib.
    }
    func setEmail(_ email:String){
        
        let disclaimerViewController = self.viewControllers[0] as! DisclaimerViewController
        disclaimerViewController.email = email
    }
    func setTos(_ tos:String){
        
        let disclaimerViewController = self.viewControllers[0] as! DisclaimerViewController
        disclaimerViewController.setTosField(tos)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
