import Foundation

class Conversions {
    
    // Convert from F to C (Integer)
    static func fahrenheitToCelsius(_ tempInF:Int) ->Int {
        let celsius = (tempInF - 32) * (5/9)
        return celsius as Int
    }
    
    // Convert from F to C (Double)
    static func fahrenheitToCelsius(_ tempInF:Double) ->Double {
        let celsius = (tempInF - 32.0) * (5.0/9.0)
        return celsius as Double
    }
    
    // Convert from C to F (Integer)
    static func celsiusToFahrenheit(_ tempInC:Int) ->Int {
        let fahrenheit = (tempInC * 9/5) + 32
        return fahrenheit as Int
    }
    
    // Convert from C to F (Integer)
    static func celsiusToFahrenheit(_ tempInC:Double) ->Double {
        let fahrenheit = (tempInC * 9.0/5.0) + 32.0
        return fahrenheit as Double
    }
    
    // Convert from miles to kilometers (Integer)
    static func milesToKilometers(_ speedInMPH:Int) ->Int {
        let speedInKPH = Double(speedInMPH)*1.60934
        return Int(speedInKPH)
    }
    
    // Convert from miles to kilometers (Double)
    static func milesToKilometers(_ speedInMPH:Double) ->Double {
        let speedInKPH = speedInMPH*1.60934
        return speedInKPH as Double
    }
    
    // Convert from kilometers to miles (Integer)
    static func kilometersToMiles(_ speedInKPH:Int) ->Int {
        let speedInMPH = Double(speedInKPH) / 1.60934
        return Int(speedInMPH)
    }
    
    // Convert from kilometers to miles (Double)
    func kilometersToMiles(_ speedInMPH:Double) ->Double {
        let speedInKPH = speedInMPH / 1.60934
        return speedInKPH as Double
    }
    
    // Convert from inches to cm
    func inchesToCentimeters(_ depthInInches:Double) ->Double {
        let depthInCentimeters = depthInInches * 2.54
        return depthInCentimeters as Double
    }
    
    // Convert from cm to inches
    func centimetersToInches(_ depthInCentimeters:Double) ->Double {
        let depthInInches = depthInCentimeters / 2.54
        return depthInInches as Double
    }
    
}
