//
//  User+CoreDataProperties.swift
//  
//
//  Created by praveen kumar on 07/09/16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var officername: String?
    @NSManaged var firmname: String?
    @NSManaged var email: String?
    @NSManaged var mobileno: NSNumber?
    @NSManaged var appid: String?
    @NSManaged var password: String?

}
