//
//  Constant.swift
//  muv
//
//  Created by praveen kumar on 09/08/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

protocol WebServiceDelegate {
    func processWebService(_ data: Data?, response: URLResponse?, error: Error?,identity: NSString)
}
class Constant{

    var webServiceDelegate: WebServiceDelegate!
    static let moc = DataController().managedObjectContext
    static var appId=""
    static let NOW = 1
    static let LATER = 2
    static let PICKUP = 1
    static let DROP = 2
    static let CUSTOMER = 1
    static let MOVER = 2
    static let CORPORATE = 3
    static let WALK = 1
    static let BIKE = 2
    static let CAR = 3
    static let TRUCK = 4
    static let ASSIGNED = 1
    static var requestViewController: RequestViewController?
    static var currentViewController: UIViewController?
    static let defaults = UserDefaults.standard
    

    static func drawBottomBorderTextField(_ myTextField: UITextField){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0.0, y: myTextField.frame.height - 1, width: myTextField.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor.black.cgColor
        myTextField.borderStyle = UITextBorderStyle.none
        myTextField.layer.addSublayer(bottomLine)
        
        if let placeholder = myTextField.placeholder {
            myTextField.attributedPlaceholder = NSAttributedString(string:placeholder, attributes: [NSForegroundColorAttributeName: UIColor.gray])
        }
        
    }
    static func setCurrentController(_ currentViewController: UIViewController){
        Constant.currentViewController = currentViewController
    }
    
    static func drawRectBorderButton(_ button: UIButton){
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    static func hexStringToUIColor (_ hex:String,alpha: Float) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = cString.substring(from: cString.characters.index(cString.startIndex, offsetBy: 1))
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }
    
    static func showActivityIndicatory(_ uiView: UIView,actInd: UIActivityIndicatorView,loadingView: UIView,container: UIView) {
        //dispatch_async(dispatch_get_main_queue()){
            
            //let container: UIView = UIView()
            container.frame = uiView.frame
            container.center = uiView.center
            container.backgroundColor = Constant.hexStringToUIColor("#ffffff", alpha: 0.3)
            
            loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
            loadingView.center = uiView.center
            loadingView.backgroundColor = Constant.hexStringToUIColor("#444444", alpha: 0.9)
            loadingView.clipsToBounds = true
            loadingView.layer.cornerRadius = 10
            
            actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
            actInd.activityIndicatorViewStyle =
                UIActivityIndicatorViewStyle.whiteLarge
            actInd.center = CGPoint(x: loadingView.frame.size.width / 2,
                                        y: loadingView.frame.size.height / 2);
            loadingView.addSubview(actInd)
            container.addSubview(loadingView)
            uiView.addSubview(container)
            actInd.startAnimating()
       // }
    }
    func uploadImageAndData(parameters: Dictionary<String, String>,urlString: NSString,identity: NSString,image: UIImage){
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                if let imageData = UIImageJPEGRepresentation(image, 0.6) {
                    //multipartFormData.append(data: imageData, name: "image", fileName: "file.png", mimeType: "image/png")
                    multipartFormData.append(imageData, withName: "uploaded_file", fileName: "swift_file.jpeg", mimeType: "image/jpeg")
                }
                for (key, value) in parameters {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
            }, to: (urlString as? String)!)
        { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseString { response in
                    //self.delegate?.showSuccessAlert()
                    print(response.request)  // original URL request
                    print(response.response) // URL response
                    print(response.data)     // server data
                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    print("Response Data ")
                    print(response.data)
                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        //self.webServiceDelegate?.processWebServicePto(JSON,identity:identity)
                        self.webServiceDelegate?.processWebService(response.data, response: response.response, error: nil, identity: identity)
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
                self.webServiceDelegate?.processWebService(nil, response: nil, error: encodingError, identity: identity)
            }
            
        }
    }
    func getPostWebServiceResponse(_ params:Dictionary<String, String>,urlString: NSString,identity: NSString){
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        //let urlString = NSString(format: “your URL”);
        print("url string is \(urlString)")
        var request : URLRequest = URLRequest(url: URL(string: NSString(format: "%@", urlString)as String)!)
        //request.url =
        request.httpMethod = "POST"
        request.timeoutInterval = 300
        //request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        //request.addValue("application/json", forHTTPHeaderField: "Accept")
        //request.HTTPBody  = try! NSJSONSerialization.dataWithJSONObject(params, options: [])
        var postString = ""
        for (key, value) in params {
            postString+=(postString == "" ? "" : "&")
            postString+=(key+"="+value)
        }
        print("getPostWebServiceResponse post params "+postString)
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let dataTask = session.dataTask(with: request) {(data: Data?, response: URLResponse?, error: Error?) in
            
            self.webServiceDelegate?.processWebService(data,response:response,error:error ,identity:identity)
            
        };

        dataTask.resume()
    }//func finish
    
    
    func getPostWebServiceWithImageResponse(_ params:Dictionary<String, String>,urlString: NSString,identity: NSString,image: UIImage){
        
        print("Enter getPostWebServiceWithImageResponse")
        uploadImageAndData(parameters: params,urlString: urlString,identity: identity,image: image)
        /*
        let configuration = URLSessionConfiguration.default
        let session = URLSession(configuration: configuration)
        //let urlString = NSString(format: “your URL”);
        print("url string is \(urlString)")
        var request : URLRequest = URLRequest(url: URL(string: NSString(format: "%@", urlString)as String)!)
        //let request : NSMutableURLRequest = NSMutableURLRequest()
        let boundary = generateBoundaryString()
        //request.url = URL(string: NSString(format: "%@", urlString)as String)
        request.httpMethod = "POST"
        request.timeoutInterval = 49000
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
       
        let imageData = UIImageJPEGRepresentation(image, 1)
        
        if(imageData==nil)  {
            print("Image is nil or not present")
            print(request)
            return;
        }
        
        request.httpBody = createBodyWithParameters(params, filePathKey: "uploaded_file", imageDataKey: imageData!, boundary: boundary)

        print(String(data:request.httpBody!, encoding:.utf8))
         print(imageData.debugDescription)
        let dataTask = session.dataTask(with: request) {(data: Data?, response: URLResponse?, error: Error?) in
            
            self.webServiceDelegate?.processWebService(data,response:response,error:error,identity:identity)
            
        };

        dataTask.resume()
        */
        
    }//func finish
    
    
    func createBodyWithParameters(_ parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String) -> Data {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString("--\(boundary)\r\n")
                body.appendString("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString("\(value)\r\n")
            }
        }
        
        let filename = "user-profile.jpg"
        
        let mimetype = "image/jpg"
        
        body.appendString("--\(boundary)\r\n")
        body.appendString("Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString("Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString("\r\n")
        
        
        
        body.appendString("--\(boundary)--\r\n")
        
        return body as Data
    }
    
    
    
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    static func internetNotWorking(_ controller:UIViewController){
        DispatchQueue.main.async(execute: {
            let approveString = "Internet not working"
            
            //self.dismissViewControllerAnimated(true, completion: nil)
            let alertController = UIAlertController(title: "Message", message: approveString, preferredStyle: UIAlertControllerStyle.alert)
            let confirmAction = UIAlertAction(
            title: "OK", style: UIAlertActionStyle.default) { (action) in
                // self.createPopUp(dic)
                exit(0)
            }
            alertController.addAction(confirmAction)
            controller.present(alertController, animated: true, completion: nil)
        });
    }

    static func roundCorners(_ corners:UIRectCorner, radius: CGFloat,button: UIButton) {
        let path = UIBezierPath(roundedRect: button.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        button.layer.mask = mask
    }
    
    static func getMoc() -> NSManagedObjectContext{
        return moc;
    }
    
    static func log(_ message:String, fileName: String, functionName: String, atLine: Int){
        print("\(fileName)  \(functionName)  \(atLine)   \(message)")
    }
    
    static func stringByAddingPercentEncodingForFormData(_ str: String) -> String? {
        let unreserved = "*-._"
        let allowed = NSMutableCharacterSet.alphanumeric()
        allowed.addCharacters(in: unreserved)
        
            allowed.addCharacters(in: " ")
       
        
        var encoded = str.addingPercentEncoding(withAllowedCharacters: allowed as CharacterSet)
        
            encoded = encoded?.replacingOccurrences(of: " ",
                                                                    with: "+")
        
        return encoded
    }
    
    static  func degreesToRadians(_ degrees: Double) -> Double { return degrees * M_PI / 180.0 }
    static  func radiansToDegrees(_ radians: Double) -> Double { return radians * 180.0 / M_PI }
    
    static  func computeHaversineDistanceInMiles(_ sLat: Double, sLon: Double,uLat: Double,uLng: Double) -> Double {
        
        let radius: Double = 3959.0 // Miles
        
        let deltaP = (degreesToRadians(sLat) - degreesToRadians(uLat))
        let deltaL = (degreesToRadians(sLon) - degreesToRadians(uLng))
        let a = sin(deltaP/2) * sin(deltaP/2) + cos(degreesToRadians(uLat)) * cos(degreesToRadians(sLat)) * sin(deltaL/2) * sin(deltaL/2)
        let c = 2 * atan2(sqrt(a), sqrt(1-a))
        var d = radius * c
        
        return d.roundToPlaces(2) // distance in miles rounded to 2 decimal places
    }
    static func showDialog(_ viewController: UIViewController,message: String,title: String,completionHandler: (() -> Void)?){
        DispatchQueue.main.async(execute: {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            let confirmAction = UIAlertAction(
            title: "OK", style: UIAlertActionStyle.default) { (action) in
                if completionHandler != nil {
                    completionHandler!()
                }
            }
            alertController.addAction(confirmAction)
            viewController.present(alertController, animated: true, completion: nil)
        });
    }
    
    static func anyToString(_ s: AnyObject) -> String {
        if let t = s as? String {
            return t
        }
        if let t = s as? Int {
            return String(t)
        }
        if let t = s as? Double {
            return String(t)
        }
        return String(describing: s)
    }
}
extension Double {
    mutating func roundToPlaces(_ places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension NSMutableData {
    
    func appendString(_ string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
