//
//  GoogleMapsOperation.swift
//  business
//
//  Created by praveen kumar on 12/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class GoogleMapsOperation: UIViewController,LocateOnTheMap {
    
    var googleMapsView: GMSMapView!
    
    let baseURLGeocode = "https://maps.googleapis.com/maps/api/geocode/json?"
    let baseURLDirections = "https://maps.googleapis.com/maps/api/directions/json?"
    
    var selectedRoute: Dictionary<String, AnyObject>!
    
    var overviewPolyline: Dictionary<String, AnyObject>!
    
    var originCoordinate: CLLocationCoordinate2D!
    
    var destinationCoordinate: CLLocationCoordinate2D!
    
    var markersArray: Dictionary<String,GMSMarker> = [:]
    
    var data = [Dictionary<String,AnyObject>]()
    
    var type = DeliveryMode.pickup
    
    var waypointsArray: Array<String> = []
    
    var totalDistanceInMeters = 0
    
    var totalDurationInSeconds = 0
    
    var addresses = [String]()
    
    var givenAddress: Bool = false
    var pickupOrdersCount = 0
    
    enum TravelModes: Int{
        case driving
        case walking
        case bicycling
    }
    
    enum DeliveryMode: Int{
        case pickup
        case drop
        case pickupdrop
    }
    
    var path: GMSMutablePath?
    var routePolyline:GMSPolyline?
    
    func calculateTotalDistanceAndDuration(_ legs: [Dictionary<String, AnyObject>]) {
       
       var totalDistanceInMeters = 0
       var totalDurationInSeconds = 0
        
        for leg in legs {
            totalDistanceInMeters += (leg["distance"] as! Dictionary<String, AnyObject>)["value"] as! Int
            totalDurationInSeconds += (leg["duration"] as! Dictionary<String, AnyObject>)["value"] as! Int
        }
        
       self.totalDistanceInMeters = totalDistanceInMeters
       self.totalDurationInSeconds = totalDurationInSeconds
    }
    
    func getDirections(_ origin: String!, destination: String!, waypoints: Array<String>!, travelMode: TravelModes, completionHandler: ((_ status: String, _ success: Bool) -> Void)?) {
        if let originLocation = origin {
            if let destinationLocation = destination {
                var directionsURLString = baseURLDirections + "origin=" + originLocation + "&destination=" + destinationLocation
                if let routeWaypoints = waypoints {
                    directionsURLString += "&waypoints="
                    var temp="";
                    for waypoint in routeWaypoints {
                        if temp==""{
                            temp="a"
                        }
                        else{
                            
                            directionsURLString += "|"
                        }
                        directionsURLString +=  waypoint
                    }
                }
                
                var travelModeString = ""
                
                switch travelMode {
                case TravelModes.walking:
                    travelModeString = "walking"
                    
                case TravelModes.bicycling:
                    travelModeString = "bicycling"
                    
                default:
                    travelModeString = "driving"
                }
                
                
                directionsURLString += "&mode=" + travelModeString
                
                
                print(directionsURLString)
                directionsURLString = directionsURLString.addingPercentEscapes(using: String.Encoding.utf8)!
                let directionsURL = URL(string: directionsURLString)
                DispatchQueue.main.async(execute: { () -> Void in
                    let directionsData = try? Data(contentsOf: directionsURL!)
                    do{
                        let dictionary: Dictionary<String, AnyObject> = try JSONSerialization.jsonObject(with: directionsData!, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<NSObject, AnyObject> as! Dictionary<String, AnyObject>
                        
                        let status = dictionary["status"] as! String
                        
                        if status == "OK" {
                            self.selectedRoute = (dictionary["routes"] as! [Dictionary<String, AnyObject>])[0]
                            self.overviewPolyline = self.selectedRoute["overview_polyline"] as! Dictionary<String, AnyObject>
                            
                            let legs = self.selectedRoute["legs"] as! [Dictionary<String, AnyObject>]
                            
                            let startLocationDictionary = legs[0]["start_location"] as! Dictionary<String, AnyObject>
                            self.originCoordinate = CLLocationCoordinate2DMake(startLocationDictionary["lat"] as! Double, startLocationDictionary["lng"] as! Double)
                            
                            let endLocationDictionary = legs[legs.count - 1]["end_location"] as! Dictionary<String, AnyObject>
                            self.destinationCoordinate = CLLocationCoordinate2DMake(endLocationDictionary["lat"] as! Double, endLocationDictionary["lng"] as! Double)
                            
                            let originAddress = legs[0]["start_address"] as! String
                            let destinationAddress = legs[legs.count - 1]["end_address"] as! String
                            
                            let originMarker = GMSMarker(position: self.originCoordinate)
                            originMarker.map = self.googleMapsView
                            originMarker.icon = GMSMarker.markerImage(with: UIColor.green)
                            originMarker.title = originAddress
                            
                            let destinationMarker = GMSMarker(position: self.destinationCoordinate)
                            destinationMarker.map = self.googleMapsView
                            destinationMarker.icon = GMSMarker.markerImage(with: UIColor.red)
                            if self.givenAddress == false{
                                destinationMarker.title = destinationAddress
                            }
                            else{
                                destinationMarker.title = self.addresses[self.addresses.count-1]
                            }
                            
                            if waypoints != nil && waypoints.count > 0 {
                                for waypoint in waypoints {
                                    let lat: Double = (waypoint.components(separatedBy: ",")[0] as NSString).doubleValue
                                    let lng: Double = (waypoint.components(separatedBy: ",")[1] as NSString).doubleValue
                                    
                                    let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
                                    marker.map = self.googleMapsView
                                    marker.icon = GMSMarker.markerImage(with: UIColor.purple)
                                    
                                }
                            }
                            
                            let route = self.overviewPolyline["points"] as! String
                            
                            self.path = GMSMutablePath(fromEncodedPath: route)!
                            self.routePolyline = GMSPolyline(path: self.path)
                            self.routePolyline?.map = self.googleMapsView
                            
                            self.calculateTotalDistanceAndDuration(legs)
                        }
                        else {
                            print("status")
                            //completionHandler(status: status, success: false)
                        }
                    }
                    catch {
                        print("catch")
                        
                        // completionHandler(status: "", success: false)
                    }
                })
            }
            else {
                print("Destination is nil.")
                //completionHandler(status: "Destination is nil.", success: false)
            }
        }
        else {
            print("Origin is nil")
            //completionHandler(status: "Origin is nil", success: false)
        }
    }
    
    func clearPath(){
        self.routePolyline?.map = nil
    }
    
    
    static func parseTravelMode(_ val: Int) -> TravelModes{
        if val == Constant.WALK {
            return TravelModes.walking
        }
        else if val == Constant.BIKE {
            return TravelModes.walking
        }
        return TravelModes.driving
    }
    
    func addPath(_ lat:Double,lng:Double){
        if self.path == nil {
            self.path = GMSMutablePath()
        }
        self.path!.add(CLLocationCoordinate2DMake(lat, lng))
    }
    
    func showPath(){
        self.routePolyline = GMSPolyline(path: self.path)
        self.routePolyline?.map = self.googleMapsView
    }
    
    func addMarker(_ lat:Double,lng:Double,markerId: String="0",type:Int = Constant.PICKUP,title: String = ""){
        let marker = GMSMarker(position: CLLocationCoordinate2DMake(lat, lng))
        marker.map = self.googleMapsView
        if type == Constant.PICKUP {
            marker.icon = GMSMarker.markerImage(with: UIColor.blue)
        }
        else{
            marker.icon = GMSMarker.markerImage(with: UIColor.orange)
        }
        if title != "" {
            marker.title = title
        }
        if markerId != "0" {
            self.markersArray[markerId] = marker
        }
    }
    
    
    func processMoverRequest(_ deliverytype: DeliveryMode){
        self.clearMarkers()
        self.clearPath()
        pickupOrdersCount = 0
        if deliverytype == DeliveryMode.pickupdrop {
            
            for i in 0..<data.count {
                if let type = data[i]["type"] as? Int {
                    if let lat = Double(data[i]["lat"] as! String) {
                        if let lng = Double((data[i]["lng"] as? String)!) {
                            if let address = data[i]["address"] as? String {
                                if type == Constant.PICKUP && (deliverytype == DeliveryMode.pickup || deliverytype == DeliveryMode.pickupdrop)  {
                                    //self.addPath(lat, lng: lng)
                                    self.addMarker(lat, lng: lng,markerId: "0",type: type,title: address)
                                    pickupOrdersCount += 1
                                }
                                else if type == Constant.DROP && (deliverytype == DeliveryMode.drop || deliverytype == DeliveryMode.pickupdrop)  {
                                    //self.addPath(lat, lng: lng)
                                    self.addMarker(lat, lng: lng,markerId: "0",type: type,title: address)
                                }
                            }
                        }
                    }
                }
            }
        } //deliverytype finish
        self.fitAllMarkers()
        //self.showPath()
    }
    
    func addPolyline(_ lat:Double,lng:Double){
        self.addPath(lat, lng: lng)
        self.routePolyline = GMSPolyline(path: self.path)
        self.routePolyline?.map = self.googleMapsView
    }
    
    func addCircle(_ lat: Double,lng: Double,radius: Double){
        let r=Double(Conversions.milesToKilometers(radius))*1000
        let circleCenter = CLLocationCoordinate2D(latitude: lat, longitude: lng)
        let circ = GMSCircle(position: circleCenter, radius: r)
        
        //circ.fillColor = UIColor(red: 0.35, green: 0, blue: 0, alpha: 0.05)
        circ.strokeColor = UIColor.red
        circ.strokeWidth = 5
        circ.map = self.googleMapsView;
    }
    
    func addAllCircle(_ arr: [NSDictionary]){
        
        for i in 0..<arr.count {
            self.addCircle((arr[i]["lat"] as! NSString).doubleValue,lng: (arr[i]["lng"] as! NSString).doubleValue,radius: Double((arr[i]["radius"]) as! Int))
        }
    }
    
    func clearMarkers(){
        for (markerId, _) in markersArray {
            markersArray[markerId]?.map = nil
            markersArray.removeValue(forKey: markerId)
        }
    }
    
    func fitAllMarkers() {
        var bounds = GMSCoordinateBounds()
        
        for (markerId, _) in markersArray {
            if let marker = markersArray[markerId]{
                bounds = bounds.includingCoordinate(marker.position)
            }
        }
        
        self.googleMapsView.animate(with: GMSCameraUpdate.fit(bounds))
    }
    
    func addMarkersFromWebService(_ data:[NSDictionary]){
        for i in 0..<data.count {
            if let lat = Double(data[i]["lat"] as! String) {
                if let lng = Double(data[i]["lng"] as! String)  {
                    if let markerId = (data[i]["id"] as? Int) {
                        let markerIds=String(markerId)
                        //key exists in marker
                        if self.markersArray[markerIds] != nil{
                            self.markersArray[markerIds]?.map = nil
                        }
                        self.addMarker(lat,lng: lng,markerId: markerIds)
                    }
                }
            }
        }
    } //addMarkersFromWebService finish
    
    
    func locateWithLongitude(_ lon: Double, andLatitude lat: Double, andTitle title: String) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        DispatchQueue.main.async{() -> Void in
                        let position = CLLocationCoordinate2DMake(lat, lon)
            
            let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: lon, zoom: 14)
           
            self.googleMapsView.camera=camera
            if title != ""{
                let marker = GMSMarker(position:position)
                marker.title=title
                marker.map=self.googleMapsView
            }
        }
    }
    
}
