//
//  LoginViewController.swift
//  muv
//
//  Created by praveen kumar on 23/06/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import SwiftValidator

class LoginViewController: UIViewController,WebServiceDelegate,ValidationDelegate  {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    let validator = Validator()
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    @IBOutlet weak var l4: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        // Do any additional setup after loading the view.
        loginButton.layer.cornerRadius = 5
        loginButton.layer.borderWidth = 1
        loginButton.layer.borderColor = UIColor.white.cgColor
        
        l4.isHidden=true
        validator.registerField(emailTextField, errorLabel: l4, rules: [RequiredRule(), EmailRule(message: "Invalid email")])
        validator.registerField(passwordTextField, errorLabel: l4, rules: [RequiredRule(), MinLengthRule(length: 6), MaxLengthRule(length: 20)])
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    @IBAction func hideKeyboard(_ sender: AnyObject) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        Constant.drawBottomBorderTextField(emailTextField)
        Constant.drawBottomBorderTextField(passwordTextField)
        //initPlot()
    }
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    func createPopUp(_ dic: NSDictionary){
         DispatchQueue.main.async(execute: {
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "otpverification") as! PopUpViewController
            self.addChildViewController(popOverVC)
            
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
            let newString = "0"
            popOverVC.mobileno = newString
            popOverVC.email = self.emailTextField.text!
            popOverVC.identity = "ae"
            popOverVC.processRegistration(dic)
         });
    }
    
    func processWebService(_ data: Data?, response: URLResponse?, error: Error?,identity: NSString){
        stopActivityIndicator()
        
        // 1: Check HTTP Response for successful GET request
        guard let httpResponse = response as? HTTPURLResponse, let _ = data
            else {
                print("error: not a valid http response")
                let errorString = "Internet not working"
                DispatchQueue.main.async(execute: {
                     self.stopActivityIndicator()
                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                    let confirmAction = UIAlertAction(
                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                        exit(0)
                    }
                    alertController.addAction(confirmAction)
                    self.present(alertController, animated: true, completion: nil)
                });
                return
        }
        
        switch (httpResponse.statusCode)
        {
        case 200:
            
            //let response = NSString (data: receivedData, encoding: NSUTF8StringEncoding)
            
            if identity == "lo" {
                do{
                    if data != nil {
                        let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                        print(dic)
                        let status = dic["status"] as! String
                        print(status)
                        if dic["pto"] == nil {
                            if status == "fail" {
                                let errorString = dic["error"] as! String
                                DispatchQueue.main.async(execute: {
                                    self.stopActivityIndicator()

                                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                                    let confirmAction = UIAlertAction(
                                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                                        exit(0)
                                    }
                                    alertController.addAction(confirmAction)
                                    self.present(alertController, animated: true, completion: nil)
                                });
                            }
                        }
                        else{
                            if status == "success" {
                                let pto = dic["pto"] as! String
                                if pto ==  "vs" {
                                    let dicData = dic["data"] as! Dictionary<String,AnyObject>
                                    LoginModel.email = dicData["email"] as! String
                                    LoginModel.mobileno = dicData["mobileno"] as! String
                                    LoginModel.firstname = dicData["firstname"] as! String
                                    LoginModel.lastname = dicData["lastname"] as! String
                                    LoginModel.address = dicData["address"] as! String
                                    LoginModel.sessionkey = dicData["sessionkey"] as! String
                                    LoginModel.lat = Double(dicData["lat"] as! String)!
                                    LoginModel.lng = Double(dicData["lng"] as! String)!
                                    LoginModel.meterupdatetime = dicData["meterupdatetime"] as! Int
                                    
                                    LoginModel.moverstatus = Int((dicData["moverstatus"] as! String))!
                                    LoginModel.subscribedmode = Int((dicData["subscribedmode"] as! String))!
                                    LoginModel.saveInDefaults()
                                    OrderWebService.storyboardHandler(self,pto: "allvehicles")
                                    
                                }
                                else if pto ==  "lo" {
                                    
                                }
                                else{
                                    self.createPopUp(dic)
                                }
                                
                            }
                            else {
                                let errorString = dic["error"] as! String
                                DispatchQueue.main.async(execute: {
                                    self.stopActivityIndicator()
                                    let pto = dic["pto"] as! String

                                    let alertController = UIAlertController(title: "Error", message: errorString, preferredStyle: UIAlertControllerStyle.alert)
                                    let confirmAction = UIAlertAction(
                                    title: "OK", style: UIAlertActionStyle.default) { (action) in
                                        if pto ==  "lo" {
                                            
                                        }
                                        else{
                                            self.createPopUp(dic)
                                        }
                                    }
                                    alertController.addAction(confirmAction)
                                    self.present(alertController, animated: true, completion: nil)
                                });
                            }
                        }
                    }
                }//if data finsihh
                catch{
                    print("reg error response")
                    Constant.internetNotWorking(self)
                }
                
            }//reg finish
            
            
        default:
            print("save profile POST request got response \(httpResponse.statusCode)")
        }
    }

    
    func validationSuccessful() {
            // submit the form
            Constant.showActivityIndicatory(self.view,actInd: self.actInd,loadingView: self.loadingView,container: self.container)
            DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
                let c = Constant()
                c.webServiceDelegate=self
               User.updateAppId(Constant.appId)
                let dictionary: [String:String] = [
                    "email" : self.emailTextField.text!,
                    "password" : self.passwordTextField.text!,
                    "medium" : String(3),
                    "appid"  : Constant.appId
                ]
                
                
                c.getPostWebServiceResponse(dictionary, urlString: RouteUrl.lo as NSString,identity: "lo")
            });
       
    }
    
    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        // turn the fields to red
        for (field, error) in errors {
           // if let field = field as? UITextField {
                //field.layer.borderColor = UIColor.redColor().CGColor
                //field.layer.borderWidth = 1.0
           // }
            error.errorLabel?.text = error.errorMessage // works if you added labels
            error.errorLabel?.isHidden = false
        }
    }
    

    @IBAction func loginClicked(_ sender: AnyObject) {
        validator.validate(self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
