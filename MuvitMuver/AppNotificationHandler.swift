//
//  AppNotificationHandler.swift
//  business
//
//  Created by praveen kumar on 11/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit

class AppNotificationHandler {
    
    
    static func handleAddressUpdateNotification(_ notificationData: [String: AnyObject]){
        
        if let orderid = notificationData["orderid"] as? String {
            if let address = notificationData["orders"] as? String{
                if let lat = notificationData["lat"] as? String{
                    if let lng = notificationData["lng"] as? String{
                        if let requestViewController = Constant.requestViewController {
                            requestViewController.updateAddress(orderid, address: address, lat: Double(lat)!, lng: Double(lng)!)
                        }
                    }
                }
            }
        }
    }
    
    static func handleOrderDroppedNotification(_ notificationData: [String: AnyObject]){
        
        if let orderid = notificationData["orderid"] as? String {
            if let requestViewController = Constant.requestViewController {
                if requestViewController.pageState == RequestViewController.PageState.accepted{
                    requestViewController.orderDropped(orderid)
                }
                
            }
        }
    }
    
    static func handleMoverRequestNotification(_ notificationData: [String: AnyObject]){
        
        let data = notificationData
        if let requestViewController = Constant.requestViewController {
            if requestViewController.pageState == RequestViewController.PageState.normal{
                requestViewController.addMoverRequest(data)
            }
        }
        
    }
    
    static func handleNotification(_ notificationData: [String: AnyObject]){
        
        if let gcmtype = notificationData["gcmtype"] as? String {
            if gcmtype == "addressupdate" {
                handleAddressUpdateNotification(notificationData)
            }
            else if gcmtype == "orderdropped" {
                handleOrderDroppedNotification(notificationData)
            }
            else if gcmtype == "moverrequest" {
                handleMoverRequestNotification(notificationData)
            }
        }
    }
    
}
