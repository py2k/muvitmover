    //
//  RequestViewController.swift
//  MuvitMuver
//
//  Created by praveen kumar on 19/09/16.
//  Copyright © 2016 Muvit Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class RequestViewController: UIViewController, CLLocationManagerDelegate, UIImagePickerControllerDelegate,UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate {
    
    //var cells: [LiquidFloatingCell] = []
    //var floatingActionButton: LiquidFloatingActionButton!
    var googleMapsView: GMSMapView!
    var data = Dictionary<String,AnyObject>()
    var orders = [Dictionary<String,AnyObject>]()
    var acceptedData = Dictionary<String,AnyObject>()
    var currentMobileNo=""
    public var deliveryImage:UIImage?
    var currentData : Dictionary<String,AnyObject>!
    var pickupData : Dictionary<String,AnyObject>?
    var gmo = GoogleMapsOperation()
    var isFinal = 0
    var bigOrderId = 0
    var usertype = 3
    var pickupOrdersCount = 0
    
    @IBOutlet weak var tableViewFooter: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var estimatedCostText: UILabel!
    @IBOutlet weak var estimatedCost: UILabel!
    @IBOutlet weak var estimatedDistance: UILabel!
    @IBOutlet weak var estimatedDistanceText: UILabel!
    @IBOutlet weak var estimatedView: UIView!
    
    var mainOrderId = 0
    var pageState = PageState.normal
    var requestCount=0
    var totalRequestCount=0
    var orderid = ""
    var cancelIndex = 0
    var updateIndex = 0
    var pastLat = 0.0
    var pastLng = 0.0
    var miles: Double = 0.0
    
    @IBOutlet weak var callMe: UIBarButtonItem!
    
    @IBOutlet weak var moveButton: UIButton!
    @IBOutlet weak var rejectButton: UIButton!
    
    enum PageState: Int{
        case request
        case accepted
        case normal
    }
    
    @IBOutlet weak var estimateddistancetime: UILabel!
    @IBOutlet weak var estiimatedbelowview: UIView!
    @IBOutlet weak var googleMapsContainer: UIView!
    
    
    let locationManager=CLLocationManager()
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    
    func clearVariables(){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        //self.cells = []
        data = Dictionary<String,AnyObject>()
        acceptedData = Dictionary<String,AnyObject>()
        currentMobileNo=""
        deliveryImage = nil
        currentData = Dictionary<String,AnyObject>()
        isFinal = 0
        pageState = PageState.normal
        requestCount=0
        totalRequestCount=0
        bigOrderId = 0
        orders = [Dictionary<String,AnyObject>]()
        orderid = ""
        cancelIndex = 0
        updateIndex = 0
        pastLat = 0.0
        pastLng = 0.0
        miles = 0.0
        mainOrderId = 0
        //gmo = GoogleMapsOperation()
        pickupOrdersCount = 0
        
        DispatchQueue.main.async{
            self.estimateddistancetime.isHidden = true
            self.estiimatedbelowview.isHidden = true
            //self.bottomRightButton?.isHidden = true
            
            self.tableView.isHidden = true
            self.estimatedCostText.isHidden = true
            self.estimatedCost.isHidden = true
            self.estimatedDistance.isHidden = true
            self.estimatedDistanceText.isHidden = true
            self.estimatedView.isHidden  = true
            self.moveButton.isHidden = true
            self.callMe.isEnabled = true
            self.callMe.title = ""
            self.rejectButton.isHidden = true
            self.moveButton.setTitle("Move", for: UIControlState())
            self.rejectButton.setTitle("Reject", for: UIControlState())
            self.tableViewFooter.isHidden = true
        }
    }
    func showAllElements(){
        self.estimateddistancetime.isHidden = false
        self.estiimatedbelowview.isHidden = false
        //bottomRightButton?.isHidden = false
        
        self.tableView.isHidden = false
        self.estimatedCostText.isHidden = false
        self.estimatedCost.isHidden = false
        self.estimatedDistance.isHidden = false
        self.estimatedDistanceText.isHidden = false
        self.estimatedView.isHidden  = false
        self.moveButton.isHidden = false
        self.callMe.isEnabled = false
        self.callMe.title = ""
        self.rejectButton.isHidden = false
        self.moveButton.setTitle("Move", for: UIControlState())
        self.rejectButton.setTitle("Reject", for: UIControlState())
    }
    
    func normalRequest(){
        clearVariables()
        pageState=PageState.normal
        self.navigationItem.title = "Wating For Request"
        self.stopActivityIndicator()
    }
    
    func initMoverRequest(){
        clearVariables()
        pageState=PageState.request
        self.navigationItem.title = "Muvit Request"
        
        DispatchQueue.main.async{
        //self.bottomRightButton?.isHidden = false
        self.estimatedCostText.isHidden = false
        self.estimatedCost.isHidden = false
        self.estimatedDistance.isHidden = false
        self.estimatedDistanceText.isHidden = false
        self.estimatedView.isHidden  = false
        
        self.moveButton.isHidden = false
        self.rejectButton.isHidden = false
        }
    }
    func initMoverAcceptRequest(){
        //clearVariables()
        pageState=PageState.accepted
        
        DispatchQueue.main.async{
        self.callMe.title = "Call"
        self.callMe.isEnabled = true
        self.estimateddistancetime.isHidden = false
        self.estiimatedbelowview.isHidden = false
        //if self.bottomRightButton != nil {
        //    self.bottomRightButton!.isHidden = true
        //}
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        Constant.requestViewController = self
        
        if revealViewController() != nil {
            menuButton.target = revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        else{
            print("reveal view controller is not present")
        }
        //let order:Dictionary<String,AnyObject> = ["orderid":"220","address":"ass"]
        //self.orders.append(order)
        //self.tableView.backgroundColor = UIColor.whiteColor()
        
        pageState = PageState.normal
        self.estimatedView.layer.borderWidth = 1
        self.estimatedView.layer.borderColor = UIColor.black.cgColor
        
        
        if self.googleMapsView == nil {
            self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
            self.googleMapsView.settings.myLocationButton=true
            self.googleMapsView.isMyLocationEnabled=true
            self.googleMapsContainer.addSubview(self.googleMapsView)
            gmo.googleMapsView = self.googleMapsView
        }
        self.clearVariables()
        //showAllElements()
        self.locationManager.delegate=self
        self.locationManager.desiredAccuracy=kCLLocationAccuracyNearestTenMeters
        if #available(iOS 9.0, *) {
            self.locationManager.allowsBackgroundLocationUpdates = true
        } else {
            // Fallback on earlier versions
        }
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        /*
        //        self.view.backgroundColor = UIColor(red: 55 / 255.0, green: 55 / 255.0, blue: 55 / 255.0, alpha: 1.0)
        // Do any additional setup after loading the view, typically from a nib.
        let createButton: (CGRect, LiquidFloatingActionButtonAnimateStyle) -> LiquidFloatingActionButton = { (frame, style) in
            let floatingActionButton = CustomDrawingActionButton(frame: frame)
            floatingActionButton.animateStyle = style
            floatingActionButton.dataSource = self
            floatingActionButton.delegate = self
            return floatingActionButton
        }
        
        let customCellFactory: (String,String) -> LiquidFloatingCell = { (iconName,name) in
            let cell = CustomCell(icon: UIImage(named: iconName)!, name: name)
            return cell
        }
        cells.append(customCellFactory("ic_place","Pick Ups"))
        cells.append(customCellFactory("ic_place","Drop Offs"))
        
        let floatingFrame = CGRect(x: self.view.frame.width - 36 - 16, y: self.view.frame.height - 86 - 16, width: 36, height: 36)
        //bottomRightButton = createButton(floatingFrame, .up)
        
        let image = UIImage(named: "ic_art")
        //bottomRightButton!.image = image
        
        //self.view.addSubview(bottomRightButton!)
        //bottomRightButton!.isHidden = true
        */
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let lat = self.locationManager.location?.coordinate.latitude
        let lon = self.locationManager.location?.coordinate.longitude
        if lat != nil && lon != nil {
            LoginModel.currentLat = lat!
            LoginModel.currentLng = lon!
        }
        self.orderid = ""
        if lat != nil && lon != nil {
            if pastLat != 0.0 && pastLng != 0.0 {
                if pageState == PageState.accepted{
                    miles+=Constant.computeHaversineDistanceInMiles(pastLat, sLon: pastLng, uLat: lat!, uLng: lon!)
                    gmo.locateWithLongitude(lon!, andLatitude: lat!, andTitle: "")
                    self.orderid = currentData["orderid"] as! String
                }
                else{
                    miles = 0.0
                    if pageState != PageState.request{
                        gmo.locateWithLongitude(lon!, andLatitude: lat!, andTitle: "")
                    }
                }
            }
            LoginModel.currentLat = lat!
            LoginModel.currentLng = lon!
            pastLat=lat!
            pastLng=lon!
            
            let orderWebService = OrderWebService()
            let dic: [String:String] = [
                "lat" : String(describing: lat),
                "lng" : String(describing: lon),
                "orderid" : self.orderid
            ]
            //orderWebService.executeWebService(self, dic: dic, identity: "uml",showActivity: 0)
            //self.locationManager.stopUpdatingLocation()
            
        
        }
    }
   
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
        
        
        if self.googleMapsView == nil {
            self.googleMapsView = GMSMapView(frame: self.googleMapsContainer.frame)
            self.googleMapsView.settings.myLocationButton=true
            self.googleMapsView.isMyLocationEnabled=true
            self.googleMapsContainer.addSubview(self.googleMapsView)
            gmo.googleMapsView = self.googleMapsView
        }
        gmo.googleMapsView = self.googleMapsView
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    func numberOfCells(_ liquidFloatingActionButton: LiquidFloatingActionButton) -> Int {
        return cells.count
    }
    
    func cellForIndex(_ index: Int) -> LiquidFloatingCell {
        return cells[index]
    }
    
    func liquidFloatingActionButton(_ liquidFloatingActionButton: LiquidFloatingActionButton, didSelectItemAtIndex index: Int) {
        print("did Tapped! \(index)")
        liquidFloatingActionButton.close()
        if index == 0 {
             gmo.processMoverRequest(GoogleMapsOperation.DeliveryMode.pickup)
        }
        else{
             gmo.processMoverRequest(GoogleMapsOperation.DeliveryMode.drop)
        }
    }*/
    
    func captureImage(){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
    
        let imageFromSource = UIImagePickerController()
        imageFromSource.delegate = self
        imageFromSource.allowsEditing = false
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imageFromSource.sourceType = UIImagePickerControllerSourceType.camera
        }
        else{
            imageFromSource.sourceType = UIImagePickerControllerSourceType.photoLibrary
        }
        self.present(imageFromSource, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        self.dismiss(animated: true, completion: nil)
        let temp: UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        self.deliveryImage = temp
        if deliveryImage == nil {
            //show error
            Constant.showDialog(self,message: "Photo not selected",title: "Error",completionHandler: nil)
        }
        else{
            
            if (currentData["type"] as! Int) == Constant.PICKUP{
                let orderWebService = OrderWebService()
                let dic: [String:String] = [
                    "orderid" : String(describing: self.currentData["orderid"]!)
                ]
                orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "cp")
            }
            else{
                let orderWebService = OrderWebService()
                let dic: [String:String] = [
                    "orderid" : String(describing: self.currentData["orderid"]!),
                    "miles" : String(self.miles),
                    "isfinal" : String(isFinal)
                ]
                orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "fco")
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
      
    }
    
    @IBAction func rejectClicked(_ sender: AnyObject) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        if self.pageState == PageState.request {
            self.normalRequest()
        }
        else if self.pageState == PageState.accepted {
            self.tableView.reloadData()
            DispatchQueue.main.async{
                self.tableView.isHidden = false
                self.tableViewFooter.isHidden = false
            }
        }
    }
    
    @IBAction func movePressed(_ sender: UIButton) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        print(self.pageState)
        if self.pageState == PageState.request {
            if mainOrderId != 0{
                let orderWebService = OrderWebService()
                let dic: [String:String] = [
                    "orderid" : String(mainOrderId),
                    "usertype" : String(usertype)
                ]
                orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "aco")
            }            
        }
        else if self.pageState == PageState.accepted {
            
            captureImage()
           
        }//accept
    }
    
    @IBAction func callButton(_ sender: AnyObject) {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let str = "tel://" + currentMobileNo
        UIApplication.shared.openURL(URL(string: str)!)
    }
    
    func addMoverRequest(_ data: Dictionary<String,AnyObject>){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        print(data)
        initMoverRequest()
        self.data = data
        self.orders = self.data["orders"] as! [Dictionary<String,AnyObject>]
        estimatedCost.text! = "$ " + String(describing: data["estimatedfare"]!)
        estimatedDistance.text! = String(describing: data["estimateddistance"]!)
        usertype = data["usertype"] as! Int
        mainOrderId = Int(data["mainorderid"] as! String)!
        gmo.data = data["orders"] as! [Dictionary<String,AnyObject>]
        gmo.processMoverRequest(GoogleMapsOperation.DeliveryMode.pickupdrop)
        
    }
    
    func processDeliveryType(){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        print(requestCount)
        print(self.orders)
        print(self.acceptedData)
        var current = self.orders[requestCount]
        currentData = Dictionary<String,AnyObject>()
        currentData["orderid"] = current["orderid"]
        currentData["lat"] = current["lat"]
        currentData["lng"] = current["lng"]
        currentData["address"] = current["address"]
        currentData["type"] = current["type"]
        currentData["name"] = (self.acceptedData[Constant.anyToString(current["orderid"]!)]! as! Dictionary<String,AnyObject>)["name"]
        currentData["mobileno"] = (self.acceptedData[Constant.anyToString(current["orderid"]!)]! as! Dictionary<String,AnyObject>)["mobileno"]
        currentMobileNo = String(describing: currentData["mobileno"]!)
        
        currentData["prevlat"] = LoginModel.currentLat as AnyObject?
        currentData["prevlng"] = LoginModel.currentLng as AnyObject?
        gmo.givenAddress = true
        gmo.addresses = [currentData["address"] as! String]
        gmo.clearPath()
        gmo.clearMarkers()
        gmo.getDirections((String(describing: currentData["lat"]!))+","+(String(describing: currentData["lng"]!)), destination: (String(describing: currentData["prevlat"]!))+","+((String(describing: currentData["prevlng"]!))), waypoints: Array(), travelMode: GoogleMapsOperation.parseTravelMode(LoginModel.currentMode), completionHandler: nil)
        //locate to current point
        gmo.locateWithLongitude(Double(String(describing: currentData["prevlat"]!))!, andLatitude: Double(String(describing: currentData["prevlng"]!))!, andTitle: "")
        
        
        DispatchQueue.main.async{
            self.navigationItem.title = self.currentData["address"] as? String
            self.estimateddistancetime.isHidden = false
            self.estiimatedbelowview.isHidden = false
            let minTime: Int = self.gmo.totalDurationInSeconds/60
            var minDistance: Double = (Double(self.gmo.totalDistanceInMeters)/1609.344)
            self.estimateddistancetime.text = String(minTime) + " mins (" + String(minDistance.roundToPlaces(2)) + " miles)"
            
            self.estimatedCostText.isHidden = true
            self.estimatedCost.isHidden = true
            self.estimatedDistance.isHidden = true
            self.estimatedDistanceText.isHidden = true
            self.estimatedView.isHidden  = true
            
            if Int(Constant.anyToString(self.currentData["type"]!))! == Constant.PICKUP {
                self.moveButton.setTitle("PICK ORDER: "+(String(describing: self.currentData["orderid"]!))+"", for: UIControlState())
                self.rejectButton.setTitle("CANCEL ORDER", for: UIControlState())
            }
            else{
                self.moveButton.setTitle("SET DROP OFF (LABEL ID : "+(String(describing: self.currentData["orderid"]))+")", for: UIControlState())
                self.rejectButton.setTitle("CANCEL ORDER", for: UIControlState())
            }
            self.callMe.isEnabled = true
            self.callMe.title = "Call"
        }
        orderid = String(describing: currentData["orderid"])
        requestCount+=1
        if requestCount == (orders).count {
            self.isFinal = 1
        }
        
        self.stopActivityIndicator()
    }
    
    func addMoverAccept(_ data: Dictionary<String,AnyObject>){
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        initMoverAcceptRequest()
        pageState = PageState.accepted
        
        if let orders = data["orders"] {
            if let t = orders as? [Dictionary<String,AnyObject>]{
                totalRequestCount = t.count
                for i in 0..<t.count {
                    let temp = Constant.anyToString(t[i]["orderid"]!)
                    self.acceptedData[temp] = t[i] as AnyObject?
                }
                processDeliveryType()
                self.tableView.reloadData()
            }
        }
    }
    
    func processNextOrder(){
        processDeliveryType()
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        if usertype == Constant.CORPORATE {
            return (self.orders.count - gmo.pickupOrdersCount)
        }
        return self.orders.count
     }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        let cell=tableView.dequeueReusableCell(withIdentifier: "cancelcell", for: indexPath) as! CancelTableViewCell
        var row = (indexPath as NSIndexPath).row
        if usertype == Constant.CORPORATE{
            row = gmo.pickupOrdersCount + (indexPath as NSIndexPath).row
        }
        
        cell.orderAddress.text=((self.orders[row]["address"] as! String))
        cell.initMe((self.orders[row]["address"] as! String), orderid: (self.orders[row]["orderid"] as! String), parent: self, index: (indexPath as NSIndexPath).row)
        return cell
     }
    
    func cancelAction()
    {
        print("cancel");
        
        DispatchQueue.main.async{
            self.tableView.isHidden = true
            self.tableViewFooter.isHidden = true
        }
    }
    @IBAction func tableviewOkPressed(_ sender: AnyObject) {
        cancelAction()
    }
   /*
    func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        
        let footerView = UIView(frame:CGRectMake(0,0,130,30))
        
        let cancelButton = UIButton(type: .Custom)
        cancelButton.setTitle("OK", forState: .Normal)
        cancelButton.addTarget(self, action: #selector(RequestViewController.cancelAction), forControlEvents: .TouchUpInside)
        cancelButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        cancelButton.backgroundColor = UIColor.blueColor()
        cancelButton.frame = CGRectMake(self.tableView.frame.size.width/2-65, 0, 130, 30)
        
        footerView.addSubview(cancelButton)
        
        return footerView
    }
   */
    
    func didFinalOrder() -> Bool{
        
        if usertype == Constant.CORPORATE{
            if (orders.count-gmo.pickupOrdersCount) == 0 {
                self.normalRequest()
                return true
            }
        }
        else{
            if (orders.count) == 0 {
                self.normalRequest()
                return true
            }
        }
        return false
    }
    
    func changeCurrentCancelView(){
        if didFinalOrder() == false {
            processDeliveryType()
        }
    }
    
    func updateAddress(_ orderid: String, address: String, lat: Double,lng: Double){
        updateIndex = findOrderIndex(orderid)
        if updateIndex != -1 {
            if pageState == PageState.accepted {
                if String(describing: orders[updateIndex]["orderid"]) == String(describing: currentData["orderid"]) {
                    orders[updateIndex]["address"] = address as AnyObject?
                    orders[updateIndex]["lat"] = lat as AnyObject?
                    orders[updateIndex]["lng"] = lng as AnyObject?
                    requestCount-=1
                    self.processDeliveryType()
                }
                else{
                    orders[updateIndex]["address"] = address as AnyObject?
                    orders[updateIndex]["lat"] = lat as AnyObject?
                    orders[updateIndex]["lng"] = lng as AnyObject?
                }
            }
        }
        
    }
    
    func reloadDataWithArrayWhenOrderCancelled(){
        
        if pageState == PageState.accepted {
            if usertype == Constant.CORPORATE{
                cancelIndex = cancelIndex + gmo.pickupOrdersCount
            }
            requestCount-=1
            totalRequestCount-=1
            if String(describing: orders[cancelIndex]["orderid"]) == String(describing: currentData["orderid"]) {
               
                orders.remove(at: cancelIndex)
                changeCurrentCancelView()
            }
            else{
                orders.remove(at: cancelIndex)
            }
            self.tableView.reloadData()
            
        }
        self.stopActivityIndicator()
     }
    
    func findOrderIndex(_ orderid: String) -> Int{
        
        for i in 0..<orders.count {
            if String(describing: orders[i]["orderid"]) == orderid {
                return i
            }
        }
        return -1
    }
    
    func orderDropped(_ orderid: String){
    
        if pageState == PageState.accepted {
            
            let index = findOrderIndex(orderid)
            if index != -1 {
                requestCount-=1
                totalRequestCount-=1
                if String(describing: orders[index]["orderid"]) == String(describing: currentData["orderid"]) {
                    cancelIndex = index
                    orders.remove(at: index)
                    changeCurrentCancelView()
                }
                else{
                    orders.remove(at: index)
                }
                self.tableView.reloadData()
            }
        }
    }
}
