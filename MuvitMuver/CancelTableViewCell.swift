//
//  CancelTableViewCell.swift
//  MuvitMuver
//
//  Created by praveen kumar on 23/09/16.
//  Copyright © 2016 Muvit Technologies. All rights reserved.
//

import UIKit

class CancelTableViewCell: UITableViewCell {

    @IBOutlet weak var orderAddress: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    var parent: RequestViewController?
    
    var orderid: String = ""
    var index = 0
    
    func initMe(_ address: String,orderid: String,parent: RequestViewController,index: Int){
        self.orderAddress.text = address
        self.orderid = orderid
        self.parent = parent
        self.index = index
    }
    
    @IBAction func cancelPressed(_ sender: AnyObject) {
        if self.parent != nil {
            let orderWebService = OrderWebService()
            let dic: [String:String] = [
                "orderid" : self.orderid
            ]
            parent?.cancelIndex = index
            orderWebService.executeWebService(parent!, dic: dic as NSDictionary, identity: "cco")
        }
    }
}
