//
//  CheckCurrentController.swift
//  business
//
//  Created by praveen kumar on 16/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import Foundation

class CheckCurrentController {
    
    
    static func newDeliveryViewController() -> Bool{
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        if let currentViewController = Constant.currentViewController {
            if currentViewController.isKind(of: NewDeliveryViewController.self) {
                return true;
            }
        }
       
        return false;
    }
    
    static func orderSummaryTableViewController() -> Bool{
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        
        if let currentViewController = Constant.currentViewController {
            if currentViewController.isKind(of: OrderSummaryTableViewController.self){
                return true;
            }
        }
        return false;
    }
    
    static func launchScreenViewController() -> Bool {
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        
        if let currentViewController = Constant.currentViewController {
            if currentViewController.isKind(of: LaunchScreenViewController.self) {
                return true;
            }
        }
        return false;
    }

}
