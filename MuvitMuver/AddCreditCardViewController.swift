//
//  AddCreditCardViewController.swift
//  business
//
//  Created by praveen kumar on 12/09/16.
//  Copyright © 2016 praveen kumar. All rights reserved.
//

import UIKit
import Braintree

class AddCreditCardViewController: UIViewController {
    
    @IBOutlet weak var cardNumber: UITextField!
    
    @IBOutlet weak var cvv: UITextField!
    
    @IBOutlet weak var expiration: UITextField!
    
    var clientToken:String = ""
    let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
    let loadingView: UIView = UIView()
    let container: UIView = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Constant.setCurrentController(self)
        
        let orderWebService = OrderWebService()
        let dictionary: [String:String] = [:]
        orderWebService.executeWebService(self, dic: dictionary as NSDictionary, identity: "gti")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        Constant.log("Enter ",fileName: #file, functionName: #function, atLine: #line)
        Constant.currentViewController = self
    }
    func addClientToken(_ token: String){
        self.clientToken = token
    }
    
    func stopActivityIndicator(){
        DispatchQueue.main.async{
            
            self.actInd.stopAnimating()
            self.loadingView.removeFromSuperview()
            self.container.removeFromSuperview()
        }
    }
    @IBAction func addCard(_ sender: AnyObject) {
        
        if (self.cardNumber.text != nil) && (self.cvv.text != nil) && (self.expiration.text != nil) && (self.expiration.text!.characters.count == 4){
            let braintreeClient = BTAPIClient(authorization: clientToken)!
            let cardClient =   BTCardClient(apiClient: braintreeClient)
            let card = BTCard(number: self.cardNumber.text!, expirationMonth: self.expiration.text!.substring(to: self.expiration.text!.characters.index(self.expiration.text!.startIndex, offsetBy: 2)), expirationYear: self.expiration.text!.substring(from: self.expiration.text!.characters.index(self.expiration.text!.startIndex, offsetBy: 3)), cvv: self.cvv.text!)
            let last4 = self.cardNumber.text!.substring(from: self.cardNumber.text!.characters.index(self.cardNumber.text!.endIndex, offsetBy: -4))
            cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                // Communicate the tokenizedCard.nonce to your server, or handle error
                if error == nil{
                    let orderWebService = OrderWebService()
                    let dic: [String:String] = [
                        "payment_method_nonce" : tokenizedCard!.nonce,
                        "last" : last4
                    ]
                    orderWebService.executeWebService(self, dic: dic as NSDictionary, identity: "icc")
                }
                else{
                    print(error)
                }
                
            }
        }
     
        
        
        
    }
    func closeMe() {
        self.dismiss(animated: true, completion: nil)
    }
}
